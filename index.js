import { AppRegistry } from 'react-native';
import App from './app/components/Router';

AppRegistry.registerComponent('valkyrie', () => App);
