import React, { Component } from 'react';
import {Text, TouchableOpacity,StatusBar,Dimensions,Image,View,Platform} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import { Icon } from 'native-base';
import Sidebar from './components/Sidebar';
import Map from './components/Map';
import Map1 from './components/Map1';
import DriverAndCustomer from './view/manager/DriverAndCustomer';
import TotalDeals from './view/manager/TotalDeals';
import EditAboutUs from './view/manager/EditAboutUs';
import Items from './view/manager/Items';
import AddItem from './view/manager/AddItems';
import EditItem from './view/manager/EditItems';
import MyAccount from './view/manager/MyAccount';

export const DrawerStack = DrawerNavigator(
  {

    DriverAndCustomer : {screen : DriverAndCustomer},
    Map : {screen:Map},
    TotalDeals : {screen: TotalDeals},
    EditAboutUs : {screen: EditAboutUs},
    Items : {screen: Items},
    MyAccount : {screen: MyAccount}
},
{
  contentComponent: props => <Sidebar {...props} />,
  drawerWidth:Dimensions.get('window').width
}
)
const DrawerNavigation = StackNavigator({
  DrawerStack: { screen: DrawerStack }
}, {
  headerMode: 'float',
  navigationOptions: ({navigation}) => ({
    //headerStyle: {backgroundColor: '#1E2A52' #0954DB
    headerStyle: {backgroundColor: 'white',elevation: 0,shadowOpacity: 0},
    //headerTitle:<View style={{width:'85%'}}><Image source={require('./images/logo.png')} style={Platform.OS=='ios'?{width:60,height:42,alignSelf:'center'}:{width:85,height:70,alignSelf:'center'}}/></View>,
    headerLeft: <TouchableOpacity style={{marginRight:35,width:'90%',padding:10}} onPress={() =>

      {if (navigation.state.index === 0) {
          navigation.navigate('DrawerOpen');
        } else {
          navigation.navigate('DrawerClose');
        }
      }
    }>
      <Icon name='menu'  style={{fontSize:30,color:'black' ,left:15}} /> 
    </TouchableOpacity>
  })
}

)
export const PrimaryNav = StackNavigator({
    drawerStack: { screen: DrawerNavigation },
    AddItem : {screen: AddItem},
    EditItem : {screen: EditItem},
  },{
    // Default config for all screens
     headerMode: 'none',
     initialRouteName: 'drawerStack',
  })

export default class index extends Component {
  
  render() {
   
    return (
      <PrimaryNav/>
    );
  }
}
