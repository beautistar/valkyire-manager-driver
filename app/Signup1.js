
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar
} from 'react-native';
import { Icon} from 'native-base';
import { Actions } from 'react-native-router-flux';
import Header from './components/Header1';

export default class Signup1 extends Component {
  constructor(props){
    super(props);
    this.state={
      email:'',
      username:'',
      password:'',
      phoneNumber: '',
      address:'',
      currentPage:1,
      emailnull:false,
      usernamenull:false,
      passwordnull:false,
      phoneNumbernull:false,
      addressnull:false

    }
  }
  signUp(){
   
    if(this.state.email == '' )
    {
      this.setState({emailnull:true})
    }
    else if(this.state.username == '')
    {
      this.setState({emailnull:false,usernamenull:true})
    }
    else if(this.state.password == '')
    {
      this.setState({passwordnull:true,usernamenull:false})
    }
    else if(this.state.phoneNumber == '')
    {
      this.setState({phoneNumbernull:true,passwordnull:false})
    }
    else if(this.state.address == '')
    {
      this.setState({addressnull:true,phoneNumbernull:false})
    }
    else
    {
      this.setState({emailnull:false,passwordnull:false,usernamenull:false,phoneNumbernull:false,addressnull:false})
      Actions.login()
    }
    }

  render() {
    return (
      <View style={styles.container}>
        <Header/>
          {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
          <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
        <ScrollView>
        <View style={{padding:10}}>
          <View style={{justifyContent:'center',alignItems:'center'}}>
            <Image source={require('./images/products/registration.png')}  style={{height:22.5,width:121}} />
            <Text style={{marginLeft:5,width:'80%',textAlign:'center',marginTop:20,marginBottom:20}}>Welcome to Valkyrie application. You need to register to start using the appliaction</Text>
            <View style={this.state.emailnull != true ? styles.inputView : styles.errorView}>
              <Icon name="ios-mail-outline" style={{padding:8,color:'#ea0017',fontSize:20,alignSelf:'flex-start'}}/>
              <TextInput  
                style={{width:200}}
                placeholder="Email" 
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({email:text})} />
            </View>
            <View style={this.state.usernamenull != true ? styles.inputView : styles.errorView}>
            <Icon name="ios-person-outline" style={{padding:8,fontSize:25, color:'#ea0017'}}/>
              <TextInput  
                style={{width:200,}}
                placeholder="User Name"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({username:text})} />
            </View>
            <View style={this.state.passwordnull != true ? styles.inputView : styles.errorView}>
            <Icon name="ios-key-outline" style={{padding:8,fontSize:20, color:'#ea0017'}}/>
              <TextInput  
                style={{width:200}}
                placeholder="********"
                underlineColorAndroid="transparent"
                secureTextEntry={true}
                onChangeText={(text) => this.setState({password:text})} />
            </View>
            <View style={this.state.phoneNumbernull != true ? styles.inputView : styles.errorView}>
            <Icon name="ios-call-outline" style={{padding:8,fontSize:25, color:'#ea0017'}}/>
              <TextInput  
                style={{width:200}}
                placeholder="Phone Number"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({phoneNumber:text})} />
            </View>
            <View style={this.state.addressnull != true ? styles.inputView : styles.errorView}>
            <Icon type="EvilIcons" name="location" style={{padding:6,fontSize:22, color:'#ea0017'}}/>
              <TextInput  
                style={{width:200}}
                placeholder="Address"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({address:text})} />
            </View>
          </View>
            <View style={styles.btnView}>
                <TouchableOpacity style={styles.btn} onPress={() => this.signUp()} >
                    <Text style={{color:'white',fontSize:16,fontWeight:'500',textAlign:'center'}}>Registration</Text>
                </TouchableOpacity>
                <View style={{margin:20}}>
                    <Text>Already have an account ? <Text style={{color:'#ea0017'}} onPress={()=>Actions.login()} >Sign in</Text></Text>
                </View>
                <View>
                    <View style={{flexDirection:'row',margin:5}}>
                        <Icon type="FontAwesome" name="facebook-square" style={{color:'#ea0017',fontSize:20}}/>
                        <Text style={{marginLeft:5}}>www.facebook.com/valkyrie/Laundry</Text>
                    </View>    
                    <View style={{flexDirection:'row',margin:5}}>
                        <Icon type="FontAwesome" name="instagram" style={{color:'#ea0017',fontSize:20}}/>
                        <Text style={{marginLeft:5}}>www.instagram.com/valkyrie/Laundry</Text>
                    </View> 
                </View>
            </View>
        </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    backgroundColor:'#ea0017'
  },
  indicator:{
    padding:20,

  },
  errorView:{
    flexDirection:'row',
    padding:5,
    borderColor:'#ea0017',
    borderWidth:1,
    width:'70%',
    height:50,
    marginTop:10,
    borderRadius:5
  },
  inputView:{
    flexDirection:'row',
    padding:5,
    borderWidth:1,
    width:'70%',
    height:50,
    marginTop:10,
    borderRadius:5
  },
  btnView:{
    justifyContent:'center',
    alignItems:'center',
    marginTop:15
  },
  btn:{
    backgroundColor:'#282423',
    height:50,
    width:'70%',
    justifyContent:'center',
    borderRadius:5
  }
});
