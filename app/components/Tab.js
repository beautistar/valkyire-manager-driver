
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput
} from 'react-native';
import { Icon } from 'native-base';


export default class AddItems extends Component {
  
  render() {
    return (
      <View style={styles.container}>
            <View style={{padding:10,flexDirection:'row',justifyContent:'space-between',alignItems:'center',width:'80%',alignSelf:'center'}}>
            <TouchableOpacity >
                <Icon name="home" type="FontAwesome" style={{color:'white'}} />
            </TouchableOpacity>
            <TouchableOpacity >
                <Image source={require('../images/products/tab1.png')} style={{height:25,width:25}} />
            </TouchableOpacity>
            <TouchableOpacity >
                <Image source={require('../images/products/tab2.png')} style={{height:20,width:35}} />
            </TouchableOpacity>
            </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
    bottom:0,
    position:'absolute',
    width:'100%',
    flexDirection:'row',
  },
  
});
