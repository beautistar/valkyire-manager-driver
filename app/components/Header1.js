
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView
} from 'react-native';
import {  Header, Left, Body, Right, Button, Title, Icon} from 'native-base';


export default class Head extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
        <View>
            <Header style={styles.header}>
              <Body style={{alignItems:'center'}}>
                <Image source={require('../images/products/title.png')} style={{height:41,width:120}} />
              </Body>
            </Header>
        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    backgroundColor:'#ea0017'
  },
  
});
