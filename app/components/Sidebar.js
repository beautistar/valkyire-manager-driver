
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  Image,
  AsyncStorage
} from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

export default class Sidebar extends Component {
  logout(){
    AsyncStorage.clear()
    Actions.login()
  }
  render() {
    return (
      <View style={styles.container}>
      <StatusBar backgroundColor='#ea0017'  barStyle={Platform.OS=='android'?"light-content":null}/> 
        <View style={styles.btnview}>
            <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('DriverAndCustomer')}> 
                    <Text style={styles.icontext}>Driver & Customer</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('Items')} > 
                    <Text style={styles.icontext}>Items</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('TotalDeals')}> 
                    <Text style={styles.icontext}>Total Deals</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('EditAboutUs')}> 
                    <Text style={styles.icontext}>Edit About Us</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('MyAccount')}> 
                    <Text style={styles.icontext}>My Account</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() =>this.logout()}> 
                    <Text style={styles.icontext}>Log Out</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('Map1')}> 
                    <Text style={styles.icontext}>Driver App</Text>
            </TouchableOpacity> */}
           
        </View>
        <View style={{backgroundColor:'#ea0017',flex:0.2}}></View>
       
     </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'row',
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: 'black',
  },
  icontext: {
    left:18,
    fontSize: 15,
    textAlign: 'center',
    margin: 10,
    fontWeight:'700',
    color:'white',
  },
  matirialicontext: {
    left:10,
    fontSize: 15,
    textAlign: 'center',
    margin: 10,
    fontWeight:'700',
    color:'white',
  },
  btn:{
    left:20,
    alignItems:'center',
    flexDirection:'row',
    margin:5,
},
  btnview:{
    flex:0.8,
    top:50,
  }
});
