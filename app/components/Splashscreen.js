
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  ScrollView,
  Dimensions,
  AsyncStorage
} from 'react-native';
//import {  Header, Left, Body, Right, Button, Title, Icon} from 'native-base';
import AppHeader from '../components/Header';
import { Actions } from 'react-native-router-flux';

export default class Splashscreen extends Component {
  componentDidMount(){
    setTimeout( () => {
          AsyncStorage.getItem('manager_id').then((manger_id)=>{
            if(manger_id){
              Actions.drawer()
            }
        })
        AsyncStorage.getItem('driver_id').then((driver_id)=>{
          if(driver_id){
            AsyncStorage.getItem('lat').then((lat)=>{
               AsyncStorage.getItem('long').then((long)=>{
                 console.log(lat,long)
                Actions.driverapp({id:driver_id,lat:Number(lat),long:Number(long)})
              })
            })
          }
      })
   },1500);
    
  }
  render() {
    return (
      <View style={styles.container}>
       <Image  source={require('../images/products/splashscreen.png')} style={{width:'100%',height:'100%',resizeMode:'cover'}}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
  flex: 1,
    // justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  
 
});
