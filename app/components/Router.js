import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar
} from 'react-native';
import {Router,Scene} from 'react-native-router-flux';
import Login from '../Login';
import HomeDrawer from '../index';
import AddDriver from '../view/manager/AddDriver';
import EditAndRemoveDriver from '../view/manager/EditAndRemoveDriver';
import SendOffers from '../view/manager/SendOffers';
import CustomerDetails from '../view/driver/CustomerDetails';
import DailySummary from '../view/manager/DailySummary';
import CustomerInformation from '../view/manager/CustomerInformation';
import AddItems from '../view/manager/AddItems';
import EditItems from '../view/manager/EditItems';
import Map1 from './Map1';
import FollowDriver from '../view/manager/FollowDriver';
import ForgotPassword from '../ForgetPassword';
import Items from '../view/manager/Items';


export default class Route extends Component{
  render() {
    return (
      <Router >
          <Scene key="root" hideNavBar={true}>
               
                <Scene key="login" component={Login}   initial />
                <Scene key="drawer" component={HomeDrawer} panHandlers={null} />
                <Scene key="addDriver" component={AddDriver}  />
                <Scene key="removeDriver" component={EditAndRemoveDriver}  />
                <Scene key="sendOffers" component={SendOffers}  />
                <Scene key="customerDetails" component={CustomerDetails} />
                <Scene key="dailySummary" component={DailySummary} />
                <Scene key="customerInfo" component={CustomerInformation} />
                <Scene key="addItems" component={AddItems} />
                <Scene key="editItems" component={EditItems} />
                <Scene key="driverapp" component={Map1} hideNavBar={true}/>
                <Scene key="followDriver" component={FollowDriver} />
                <sceen key='losspassword' component={ForgotPassword}/>

                <sceen key='item' component={Items}/>
          </Scene>
      </Router>
      );
  }
}
