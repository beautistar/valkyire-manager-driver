
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {  Header, Left, Body, Right, Button, Title, Icon} from 'native-base';
import { Actions } from 'react-native-router-flux';

export default class AppHeader extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
            <Header style={styles.header}>
              <Left>
              <TouchableOpacity style={{width:'80%'}} onPress={() => Actions.pop()} ><Icon name='arrow-back' /></TouchableOpacity>
              </Left>
              <Body style={{marginLeft:'25%'}}>
              <Image source={require('../images/logo.png')} style={Platform.OS=='ios'?{marginLeft:'-95%',width:60,height:42,alignSelf:'center'}:{marginLeft:'-25%', width:85,height:70,alignSelf:'center'}}/>
              </Body>
              <Right>
            
              </Right>
            </Header>
        </View>
    
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    backgroundColor:'white'
  },
  
});
