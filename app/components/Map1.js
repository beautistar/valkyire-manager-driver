import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  AsyncStorage,
  BackHandler,
  Dimensions,
  Linking,
  Alert,
  ToastAndroid
} from 'react-native';
import MapView,{Marker,MAP_TYPES, PROVIDER_DEFAULT, ProviderPropType, UrlTile } from 'react-native-maps';
import {  Header, Left, Body, Right, Button, Title, Icon,RadioButton, Thumbnail} from 'native-base';
import { Actions } from 'react-native-router-flux';
import Api from './api';
import MapViewDirections from 'react-native-maps-directions';
import Geocoder from 'react-native-geocoding';
import CustomerDetails from '../view/driver/CustomerDetails';
import getDirections from 'react-native-google-maps-directions'
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
// const origin = {latitude: 37.3318456, longitude: -122.0296002};
// const destination = {latitude: 37.771707, longitude: -122.4053769};
const GOOGLE_MAPS_APIKEY = 'AIzaSyDAurXVcn22DDV0Llzxa3RgbN-71OZPjwc';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0100;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class Map1 extends Component {
  constructor(props){
    super(props);
    this.state={
      active:false,
      address:'',
      lat:this.props.lat,
      long:this.props.long,
      marker:[],
      origin:'',
      destination :'',
      des:'',
      id:0,
      order_id:'',
      orderID:'',
      custName:'',
      totalItem:'',
      totalAmt:'',
      phone:'',
      req:0,
      btn:'Confirm Item',
      
    }
  }
  
 callapi(){
  let lat,long;
  latlong=[]
  data=[]
  flag=0
  c=0
  AsyncStorage.getItem('btn')
  .then((btn) => {if(btn){this.setState({btn:btn})} } )
  AsyncStorage.getItem('destination').then((des)=>{
    console.log('des : ',des)
    AsyncStorage.getItem('order_id').then((id)=>{
     
      this.setState({des:JSON.parse(des),order_id:id})
      console.log('order id :',id)
    })
   
  })
  Api.post('app/api/getOrderList')
  .then((resp)=>{
   console.log('data',resp.order_list)
      c=0
      data=resp.order_list
          if(data != undefined) 
          {
            for(i=0;i<data.length;i++){
              if(data[i].latitude!='0.00000000' && data[i].longitude!='0.00000000' && data[i].status!='Confirmed'){
  
              latlong[c]={cust_id:data[i].customer_id,
                cust_name:data[i].customer_name,
                invoice_number:data[i].invoice_number,
                phone:data[i].phone_number,
                order_id:data[i].order_id,
                ordered_at:data[i].ordered_at,
                total_cost:data[i].total_cost,
                total_itmes:data[i].total_itmes,
                latlng:{ latitude:Number(data[i].latitude),longitude: Number(data[i].longitude)}
              
              }
              c++
            }
          }  
        }
        this.setState({marker:latlong,req:c})
        console.log('latlong 2: ',this.state.marker)
  })

 }
      
  componentDidMount(){
    if(Platform.OS === 'android'){
      LocationServicesDialogBox.checkLocationServicesIsEnabled({ 
        message: "<h2>Use Location?</h2> \
                    This app wants to change your device settings:<br/><br/>\
                    Use GPS for location<br/><br/>", 
        ok: "YES", 
        cancel: "NO" 
    })
  }
    interval= this.locationApi()
      setInterval(() => {
        this.locationApi()
      }, 10000);
  }
  locationApi(){
    this.callapi()
    Geocoder.init('AIzaSyAXfqKtg1eihJdmbN-IugX2r3xtILh5b5Y');
    navigator.geolocation.getCurrentPosition(
      (position) => {
      lat = parseFloat(position.coords.latitude)
      long = parseFloat(position.coords.longitude)
       this.setState({lat:lat,long:long})
        this.setState({origin:{latitude:lat, longitude:long}})
       // console.log('STATE',this.state.lat,this.state.long)
       console.log('lat',lat)
       console.log('long',long)
       AsyncStorage.setItem('lat',lat.toString())
       AsyncStorage.setItem('long',long.toString())
        Geocoder.from(lat,long)
        .then(json => { 
                  //console.log(json)
                var addressComponent = json.results[0].address_components[0];
                this.setState({address:json.results[0].formatted_address})
         // console.log(addressComponent);
          })
          .catch(error => console.log('error:',error)) 
        },(error)=>{console.log('error-location:',error)
          },
        { enableHighAccuracy: true, timeout: 20000 }) 
        
        var formData = new FormData()
        //formData.append("user_id", this.props.id);
        formData.append("user_id",this.props.id)
        formData.append("latitude",this.state.lat);
        formData.append("longitude",this.state.long);
        formData.append("address",this.state.address);
          //console.log(this.props.id)
        Api.post('app/api/updateDriverLocation',formData)
        .then((resp)=>
                {
                //  console.log('data : ',resp)
                    if(resp.message == 'Success.')
                    {
                      console.log("succesfully updated...!")
                    }
            })  
  }
 
  componentWillMount(){
    this.locationApi()
    this._backPress = 0;

      BackHandler.addEventListener('backPress', () => {
        setTimeout(() => {
          this._backPress = 0;
        }, 3000);
      
        this._backPress += 1;
        if (this._backPress <= 1) {
          ToastAndroid.show('press back again to close app', ToastAndroid.SHORT);
          return true        
        }
        BackHandler.exitApp()

      });
    }

    sendSMS(url){
      this.setState({id:null})
      Linking.canOpenURL(url).then(supported => {
        if (!supported) {
         console.log('Can\'t handle url: ' + url);
        } else {
         return Linking.openURL(url);
        }
      }).catch(err => console.log('An error occurred', err));
    
   }


   _done(){
    AsyncStorage.getItem('btn2').then((btn2)=>{
      if(btn2=='Done Deal'){
        this.setState({id:null,order_id:null,orderID:null,destination:null,btn:'Confirm Item'})
        AsyncStorage.removeItem('destination')
        AsyncStorage.removeItem('btn')
        AsyncStorage.removeItem('order_id')
        var formData = new FormData()
        AsyncStorage.getItem('Oid').then((Oid)=>{
        formData.append("user_id",this.props.id)
        formData.append("item_id",Oid);
        Api.post('app/api/confirmItem',formData)
        .then((resp)=>{
          AsyncStorage.clear()
          console.log('data : ',resp)
        })

      })
      }

    })


    this.callapi() 
    this.setState({id:null,order_id:null,btn:'Done Deal'})
   
    AsyncStorage.removeItem('order_id')
   
    AsyncStorage.setItem('destination',JSON.stringify(this.state.destination))
    AsyncStorage.setItem('btn2','Done Deal');
    AsyncStorage.setItem('btn','Done Deal');
   }

   handleGetDirections(){
    const data = {
       source: {
        latitude: this.state.lat,
        longitude: this.state.long
      },
      destination: this.state.destination,
      params: [
       
        {
          key: "travelmode",
          value: "driving"        // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: "dir_action",
          value: "navigate"       // this instantly initializes navigation using the given travel mode 
        }
      ],
    }
    if(this.state.btn == 'Done Deal')
     {
      AsyncStorage.getItem('btn2').then((btn2)=>{
        if(btn2=='Done Deal'){
          this._done()  
        }
        else{
          getDirections(data) 
          this._done()  
        }
        
      })
     
     }  
     else{
      this.setState({order_id:this.state.orderID,des:this.state.destination,id:null,btn:'Done Deal'})
      AsyncStorage.setItem('destination',JSON.stringify(this.state.destination))
      AsyncStorage.setItem('order_id',this.state.orderID)
      AsyncStorage.setItem('btn','Done Deal')
      console.log('order_id : ',this.state.orderID,'destination',this.state.destination,'btn',this.state.btn)
      getDirections(data)   
     }
    
  }


  marker_cust(marker){
   
   // this.setState({id:marker.cust_id},() => {console.log(this.state.id)})
   if(this.state.order_id!= marker.order_id){
        this.setState({id :marker.cust_id, 
          phone:marker.phone, 
          orderID:marker.order_id,
          custName:marker.cust_name,
          totalAmt:marker.total_cost,
          totalItem:marker.total_itmes,
          btn:'Confirm Item',
          destination:{latitude: marker.latlng.latitude, longitude:marker.latlng.longitude},
          },() => {console.log(this.state.id)})
          AsyncStorage.getItem('btn2').then((btn2)=>{
            if(btn2=='Done Deal'){
              AsyncStorage.getItem('Oid').then((Oid)=>{
                if(Oid==marker.order_id){
                  this.setState({btn:'Done Deal'})
                  AsyncStorage.setItem('btn','Done Deal')
                }
                else
                {
                  AsyncStorage.setItem('btn','Confirm Item')
                }
              })
            }
            else{
              AsyncStorage.setItem('btn','Confirm Item')
            }
          })
         
   }
   else
   {
    this.setState({id :marker.cust_id, 
      phone:marker.phone, 
      orderID:marker.order_id,
      custName:marker.cust_name,
      totalAmt:marker.total_cost,
      totalItem:marker.total_itmes,
      destination:{latitude: marker.latlng.latitude, longitude:marker.latlng.longitude},
      btn:'Done Deal'  
    },() => {console.log(this.state.id)})
      
      AsyncStorage.setItem('destination',JSON.stringify({latitude: marker.latlng.latitude, longitude:marker.latlng.longitude}))
      AsyncStorage.setItem('btn','Done Deal')
      AsyncStorage.setItem('Oid',marker.order_id)
    }
    
   
   console.log('id',this.state.id,'order_id : ',this.state.orderID,'destination',this.state.destination)
  }

  get mapType() {
    // MapKit does not support 'none' as a base map
    return this.props.provider === PROVIDER_DEFAULT ?
      MAP_TYPES.STANDARD : MAP_TYPES.NONE;
  }

  render() {
  //setInterval(this.currentLocation.bind(this),30000) 
    var lat =this.state.lat
    var long =this.state.long
    return (
      <View style={styles.container}>
        <Header style={styles.header}>
        <StatusBar backgroundColor='#ea0017'  barStyle={Platform.OS=='android'?"light-content":null}/> 
              <Left>
              </Left>
              <Body style={{marginLeft:'25%'}}>
              <Image source={require('../images/logo.png')} style={Platform.OS=='ios'?{marginLeft:'-95%',width:60,height:42,alignSelf:'center'}:{marginLeft:'-25%', width:85,height:70,alignSelf:'center'}}/>
              </Body>
              <Right>
                <View style={{flexDirection:'row'}} ><TouchableOpacity style={{padding:3,justifyContent:'center',alignItems:'center',borderWidth:1,marginRight:5,width:70}}><Text style={{fontSize:10,color:'black'}}>{this.state.req} Request</Text></TouchableOpacity><TouchableOpacity onPress={()=>{AsyncStorage.clear(),Actions.login()}}><Icon  name='ios-log-out' style={{marginRight:5}}/></TouchableOpacity></View>
              </Right>
            </Header>
        <MapView
        initialRegion={{
          latitude:lat,
          longitude:long,
          latitudeDelta:LATITUDE_DELTA,
          longitudeDelta:LONGITUDE_DELTA ,
        }}
        //style={StyleSheet.absoluteFill}
        style={styles.map}
        ref={c => this.mapView = c}
        //onPress={()=>{this.setState({id:0})}}
        toolbarEnabled
        //mapType={MAP_TYPES.STANDARD}
        customMapStyle={customStyle}


      > 
        {/* <UrlTile
            urlTemplate="http://c.tile.stamen.com/watercolor/{z}/{x}/{y}.jpg"
            zIndex={-1}
          /> */}
        
          <MapViewDirections
            origin={this.state.origin}
            destination={this.state.des}
            apikey={GOOGLE_MAPS_APIKEY}
            strokeWidth={5}
            strokeColor="green"
            onStart={(params) => {
              console.log(`Started routing between "${params.origin}" and "${params.destination}"`);
            }}
            // onReady={(result) => {
            //   this.mapView.fitToCoordinates(result.coordinates, {
            //     edgePadding: {
            //       right: (width / 20),
            //       bottom: (height / 20),
            //       left: (width / 20),
            //       top: (height / 20),
            //     }
            //   });
            // }}
            onError={(errorMessage) => {
              this.setState({custName:''})
              console.log('GOT AN ERROR');
            }}
          />
          {this.state.marker.map((marker,index) => (
           
            <Marker 
            pinColor={this.state.order_id==marker.order_id?'blue':'red'}
            key={index}
            // onPress={()=>{this.setState({id:this.state.id != null ? null : marker.cust_id,custName:marker.cust_name,totalAmt:marker.total_cost,totalItem:marker.total_itmes})}}
             //onPress={() => Actions.customerDetails({data:marker})}
           onPress={() =>{this.marker_cust(marker)}}
            coordinate={marker.latlng}
          />
          
          ))}
          <Marker 
            coordinate={{latitude:this.state.lat ,longitude:this.state.long}}
            
            >
              <View style={{justifyContent:'center',alignItems:'center',zIndex:10}} >
                <View style={{borderWidth:3,borderRadius:50,height:38,width:38,justifyContent:'center',alignItems:'center',backgroundColor:'white',padding:1}}>
                  <Text style={{fontSize:8,fontWeight:'bold'}}>03 Min</Text>
                </View>
                <View style={{borderRightWidth:Platform.OS == 'ios' ? 1.5 : 1,height:12,borderColor:Platform.OS == 'android' ? '#ffffff' : null}} />
                <Image source={require('../images/products/marker.png')} style={{tintColor:Platform.OS == 'android' ? '#ffffff' : null,height:16,width:45}} />
              </View>
          </Marker>
          {console.log('----------btn----------',this.state.btn)}          
        </MapView>
        {/* <View style={{backgroundColor:'white'}}>
        <Text>Lat = {this.state.lat}</Text>
        <Text>Long = {this.state.long}</Text>
        </View> */}
       {
      this.state.id ? 
         
//   <View>

                    
<View style={{backgroundColor:'#ea0017',width:280,height:Platform.OS == 'ios' ? 418 : 412,borderRadius:3,marginTop:'10%'}} >

      <CustomerDetails 
      cust_name={this.state.custName}
      total_cost={this.state.totalAmt}
      total_itmes={this.state.totalItem}
      confirm={()=>{this.handleGetDirections()}}
      unConfirm={()=>this.setState({id:null})}
      call={()=>this.sendSMS('tel:'+Number(this.state.phone))}
      sms={()=>this.sendSMS('sms:'+this.state.phone)}
      btn={this.state.btn}
      close={()=>this.setState({id:null})}
      />
   </View>
   
    
: null }
        {/* <View style={{ backgroundColor: 'white', zIndex: 1, padding: 2, alignSelf: 'center', flexDirection: 'row', alignItems: 'center', marginTop: 5, borderRadius: 3,height:Platform.OS == 'ios' ? 40 : null }}>
        <Icon name={this.state.active == false ? null : 'arrow-back'} style={{ marginLeft: 15, fontSize: 22 }} />
        <TextInput placeholder="Search" onFocus={()=>this.setState({active:true})} underlineColorAndroid="transparent" style={{ width: '60%', marginLeft: 10 }} />
          <Icon name={this.state.active == false ? null : 'close'} style={{fontSize:22}} />
          <Icon name='mic' style={{marginLeft:10,paddingRight:10,fontSize: 22 }} />
        </View> */}
        {/* <View style={{top:Platform.OS == 'android' ? '40%' : '50%',alignSelf:'flex-end',margin:20}}>
          <Image source={require('../images/products/earth.png')} style={{height:25,width:25}} />
          <Image source={require('../images/products/pinlocation.png')} style={{height:25,width:25,marginTop:3}} />
        </View>
      <View style={{flexDirection:'row',bottom:0,width:'100%',alignItems:'center',position:'absolute'}} >
        <TouchableOpacity style={{ flexDirection:'row',backgroundColor:'white',alignItems:'center',padding:18,width:'50%',justifyContent:'center'}}>
          <Text style={{fontSize:22,color:'red',marginLeft:5,fontWeight:Platform.OS == 'android' ? '700' : 'bold'}}>OK</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{flexDirection:'row',backgroundColor:'red',alignItems:'center',padding:18,width:'50%',justifyContent:'center'}}>
          <Text style={{fontSize:22,color:'white',marginLeft:5,fontWeight:Platform.OS == 'android' ? '700' : 'bold'}}>CANCEL</Text>
        </TouchableOpacity>
      </View> */}
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  marginTop:Platform.OS == 'ios' ? '17%' : 0,
   height:Platform.OS == 'android' ? '100%' : '100%',
   width: '100%',
   flex: 1
  },
  header: {
    zIndex:1,
    backgroundColor:'white',
    width:'100%'
  },
});

const customStyle = [
  {
    elementType: 'geometry',
    stylers: [
      {
        color: '#242f3e',
      },
    ],
  },
  {
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#746855',
      },
    ],
  },
  {
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#242f3e',
      },
    ],
  },
  {
    featureType: 'administrative.locality',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#d59563',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#d59563',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'geometry',
    stylers: [
      {
        color: '#263c3f',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#6b9a76',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [
      {
        color: '#38414e',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#212a37',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9ca5b3',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry',
    stylers: [
      {
        color: '#746855',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#1f2835',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#f3d19c',
      },
    ],
  },
  {
    featureType: 'transit',
    elementType: 'geometry',
    stylers: [
      {
        color: '#2f3948',
      },
    ],
  },
  {
    featureType: 'transit.station',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#d59563',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [
      {
        color: '#17263c',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#515c6d',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#17263c',
      },
    ],
  },
];