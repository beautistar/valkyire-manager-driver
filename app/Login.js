import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  AsyncStorage
} from 'react-native';
import { Icon} from 'native-base';
import { Actions } from 'react-native-router-flux';
import Header from './components/Header1';
import Splashscreen from './components/Splashscreen' ;
import Api from './components/api';


export default class App extends Component {
  constructor(props){
    super(props);
    this.state={
      // email:'driver1@user.com',
      // password:'123456',
      // email:'manager@user.com',
      // password:'123456',
      email:'',
      password:'',
      emailnull:false,
      passwordnull:false,
      demo:'',
      timePassed: false,
      error:'',
      lat:0,
      long:0,
    }
  }
  componentDidMount() {
    setTimeout( () => {
       this.setTimePassed();
    },2000);

  
  }
  setTimePassed() {
    this.setState({timePassed: true});
 }
  
  login(){
     
  
      if(this.state.email != '' && this.state.password != '')
      {
        this.setState({emailnull:false,passwordnull:false})
        var formData = new FormData()
        formData.append("email", this.state.email);
        formData.append("password", this.state.password);
          
        Api.post('app/api/stuffSignin',formData)
        .then((resp)=>
                {
                  console.log('data : ',resp)
                    if(resp.message == 'Success.')
                    {
                      console.log(this.state.password)
                      //this.setState({error:'',email:'',password:''})
                        if(resp.user_model.role=='customer'){
                          AsyncStorage.setItem('email',resp.user_model.email)
                          AsyncStorage.setItem('user_name',resp.user_model.user_name)
                          AsyncStorage.setItem('address',resp.user_model.address)
                          AsyncStorage.setItem('password',this.state.password)
                          AsyncStorage.setItem('phone_number',resp.user_model.phone_number)
                          AsyncStorage.setItem('manager_id',resp.user_model.user_id)
                          AsyncStorage.removeItem('driver_id')
                          Actions.drawer()
                        }
                        if(resp.user_model.role=='driver'){
                          AsyncStorage.setItem('lat',resp.user_model.latitude)
                          AsyncStorage.setItem('long',resp.user_model.longitude)
                          AsyncStorage.setItem('driver_id',resp.user_model.user_id)
                          AsyncStorage.removeItem('manager_id')
                          Actions.driverapp({id:resp.user_model.user_id,lat:Number(resp.user_model.latitude),long:Number(resp.user_model.longitude)})
                        }
                      
                    }
                    else{
                        this.setState({error:resp.message})
                    }
                 
            })
      
      }
      else if(this.state.email == '' )
      {
        this.setState({emailnull:true})
      }
      else if(this.state.password == '')
      {
        this.setState({passwordnull:true,emailnull:false})

      }
      else
      {
        this.setState({emailnull:false,passwordnull:false})
      }
  }
  render() {
    if(!this.state.timePassed){
      return <Splashscreen/>
    }
    else{
     return (
      <View style={styles.container}>
        <Header/>
        <StatusBar backgroundColor='#ea0017'  barStyle={Platform.OS=='android'?"light-content":null}/> 
        <ScrollView style={{marginBottom:'10%'}}>
       
        <View style={styles.image}>
            <Image source={require('./images/logo.png')} style={{height:130,width:160}}/>
        </View>
        <View>
          <View style={{justifyContent:'center',alignItems:'center'}}>
          <View style={[styles.inputView,this.state.error?{borderColor:'red'}:null]}>
            <Icon name="ios-person-outline" style={{padding:8,fontSize:25,  color:'#ea0017'}}/>
              <TextInput  
                style={{width:200}}
                placeholder="Email" 
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({email:text})}
                value={this.state.email} />
            </View>
            {this.state.emailnull != false ? <Text style={{color:'red'}}>please enter email</Text>: null}
            <View style={[styles.inputView,this.state.error?{borderColor:'red'}:null]}>
            <Icon name="ios-key-outline" style={{padding:8,fontSize:20,color:'#ea0017'}}/>
              <TextInput  
                style={{width:200}}
                placeholder="********"
                underlineColorAndroid="transparent"
                secureTextEntry={true}
                onChangeText={(text) => this.setState({password:text})} 
                value={this.state.password}/>
            </View>
            {this.state.error ? <Text style={{marginTop:10,color:'red'}}>{this.state.error}</Text>: null}
          </View>
          <View style={styles.btnView}>
            <TouchableOpacity style={!this.state.email || !this.state.password ? styles.disable : styles.btn} onPress={()=>this.login()} disabled={!this.state.email || !this.state.password} >
              <Text style={{color:'white',fontSize:16,fontWeight:'300',textAlign:'center'}}>Log in</Text>
            </TouchableOpacity >
            {/* <TouchableOpacity   style={{height:'20%',margin:15}} onPress={()=> Actions.losspassword()}>
            <Text>Lost password ?</Text>
            </TouchableOpacity> */}
          </View>
        </View>
        </ScrollView>
      </View>
    );}
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  header: {
    backgroundColor:'#ea0017'
  },
  image:{
    alignItems:'center',
    margin:10
  },
  inputView:{
    flexDirection:'row',
    padding:5,
    borderWidth:1,
    width:'70%',
    height:50, 
    marginTop:10,
    borderRadius:5
  },
  btnView:{
    justifyContent:'center',
    alignItems:'center',
    marginTop:15
  },
  disable:{
    backgroundColor: 'rgba(234, 0, 23, 0.5)',
    height:50,
    width:'70%',
    justifyContent:'center',
    borderRadius:5
  },
  btn:{
    backgroundColor:'#ea0017',
    height:50,
    width:'70%',
    justifyContent:'center',
    borderRadius:5
  },
  socialBtn:{
    width:220,
    flexDirection:'row',
    backgroundColor:'#445F95',
    padding:5,
    justifyContent:'center',
    borderRadius:5
  }
});
