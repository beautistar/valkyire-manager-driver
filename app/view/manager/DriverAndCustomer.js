
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  BackHandler,
} from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Geocoder from 'react-native-geocoding';
export default class DriverAndCustomer extends Component {
    constructor(props){
        super(props);
        this.state={
            active:false,
            btn:'1',
            lat:0,
            long:0,
        }
       
       
    }


    static navigationOptions = {
        headerTitle : <View style={{width:'80%'}}><Text style={{color:'red',textAlign:'center',fontSize:18}}>DRIVER & CUSTOMER</Text></View>
    }
    componentDidMount(){
      Geocoder.init('AIzaSyAXfqKtg1eihJdmbN-IugX2r3xtILh5b5Y');
      navigator.geolocation.getCurrentPosition(
        (position) => {
        lat = parseFloat(position.coords.latitude)
        long = parseFloat(position.coords.longitude)
         this.setState({lat:lat,long:long})

        },(error)=>console.log('error:',error),
        { enableHighAccuracy: true, timeout: 20000 })
  
    }
    componentWillMount(){
      BackHandler.addEventListener('hardwareBackPress', function() {
        if (Actions.currentScene === 'drawer') {
          BackHandler.exitApp()
        }
        Actions.pop()
        return true
      });
      }
  render() {
    let {btn}=this.state;
    return (
      <View style={styles.container}>
        <ScrollView style={{width:'100%'}}>
            <View style={{marginTop:'20%',width:'100%',borderColor:'white'}}>
            
                <TouchableOpacity  style={{paddingLeft:20,flexDirection:'row',backgroundColor:btn=='1'?'red':null,padding:10,width:'100%'}} onPress={() =>{this.setState({btn:'1'}),Actions.addDriver()}}>
                    <Image source={require('../../images/products/addDriver.png')} style={{marginLeft:15,height:26,width:27}} />
                    <Text style={{marginLeft:20,color:'white',marginBottom:5,fontSize:16}} >Add Driver</Text> 
                </TouchableOpacity>
              
                <TouchableOpacity style={{paddingLeft:20,flexDirection:'row',backgroundColor:btn=='2'?'red':null,padding:10,width:'100%'}} onPress={() =>{this.setState({btn:'2'}), Actions.followDriver({lat:this.state.lat,long:this.state.long})}}>
                    <Image source={require('../../images/products/followDriver.png')} style={{marginLeft:15,height:25,width:27}} />
                    <Text style={{marginLeft:20,color:'white',marginBottom:5,fontSize:16}} >Follow Driver</Text> 
                </TouchableOpacity>
                <TouchableOpacity style={{paddingLeft:20,flexDirection:'row',backgroundColor:btn=='3'?'red':null,padding:10,width:'100%'}} onPress={() =>{this.setState({btn:'3'}), Actions.removeDriver()}} >
                    <Image source={require('../../images/products/removeDriver.png')} style={{marginLeft:15,height:22,width:22}} />
                    <Text style={{marginLeft:24,color:'white',marginBottom:5,fontSize:16}} >Remove Driver and Customer</Text> 
                </TouchableOpacity>
                <TouchableOpacity style={{paddingLeft:20,flexDirection:'row',backgroundColor:btn=='4'?'red':null,padding:10,width:'100%'}} onPress={() => {this.setState({btn:'4'}),Actions.sendOffers()}}>
                    <Image source={require('../../images/products/offers.png')} style={{marginLeft:15,height:25,width:27}} />
                    <Text style={{marginLeft:19,color:'white',marginBottom:5,fontSize:16}} >Send Offers To Customers</Text> 
                </TouchableOpacity>
                <TouchableOpacity style={{paddingLeft:20,flexDirection:'row',backgroundColor:btn=='5'?'red':null,padding:10,width:'100%'}} onPress={() => {this.setState({btn:'5'}),Actions.dailySummary()}}>
                    <Image source={require('../../images/products/dailySummary.png')} style={{marginLeft:15,height:25,width:25}} />
                    <Text style={{marginLeft:21,color:'white',marginBottom:5,fontSize:16}} >Daily Summary</Text> 
                </TouchableOpacity>
                <TouchableOpacity style={{paddingLeft:20,flexDirection:'row',backgroundColor:btn=='6'?'red':null,padding:10,width:'100%'}} onPress={() => {this.setState({btn:'6'}),Actions.customerInfo()}}>
                    <Image source={require('../../images/products/customerinfo.png')} style={{marginLeft:15,height:25,width:25}} />
                    <Text style={{marginLeft:21,color:'white',marginBottom:5,fontSize:16}} >Customer Information</Text> 
                </TouchableOpacity>
                
            </View>
        </ScrollView>
      </View>
    );
  }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#000',
  },
  container2: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:20,
    fontWeight:Platform.OS == 'android' ? '500' :'900'
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  inputView:{
    flexDirection:'row',
    width:'66%',
    height:35, 
    marginLeft:20,
    borderRadius:3,
    backgroundColor:'white'
  },
 
});
