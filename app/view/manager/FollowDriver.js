import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  Modal,
  Linking,
  Dimensions
} from 'react-native';
import MapView,{Marker} from 'react-native-maps';
import Geocoder from 'react-native-geocoding';
import { Header, Left, Body, Right, Button, Title, Icon } from 'native-base';
import Api from '../../components/api';

var id=[];
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 40.0100;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class FollowDriver extends Component {
  constructor(props){
    super(props);
    this.state={
      active:false,
      modal:false,
      id:'',
      lat:this.props.lat,
      long:this.props.long,
      latDelta:LATITUDE_DELTA,
      longDelta:LONGITUDE_DELTA,
      latlng:[]
    }
  }
  static navigationOptions ={
    headerTitle : <View style={{width:'90%',alignItems:'center',justifyContent:'center'}}><Text style={{color:'red',textAlign:'center',fontSize:16}}>FOLLOW UP WITH DRIVERS</Text></View>
      }

     

      getDriverLocation(){
        latlong=[]
        c=0
        Api.post('app/api/getDriverCustomerList')
        .then((resp)=>{
          // console.log('data',resp)
          for(i=0;i<resp.user_list.length;i++){
            if(resp.user_list[i].role=='driver'){
               if(resp.user_list[i].longitude!='0.00000000' && resp.user_list[i].latitude !='0.00000000'){
                  latlong[c]= {name:resp.user_list[i].user_name,id:resp.user_list[i].user_id,address:resp.user_list[i].address,phone:resp.user_list[i].phone_number,marker:{ latitude: Number(resp.user_list[i].latitude),longitude:Number(resp.user_list[i].longitude)}}
                   c++
               }
            }
          }
           console.log('latlong : ',latlong)
          if(!resp.error_message)
          {
               this.setState({latlng:latlong})
          }
        })
      }
       componentDidMount(){
        this.getDriverLocation()
        setInterval(() => {
          this.getDriverLocation()
        }, 5000);
       }

       sendSMS(url){
         this.setState({id:null})
          Linking.canOpenURL(url).then(supported => {
            if (!supported) {
             console.log('Can\'t handle url: ' + url);
            } else {
             return Linking.openURL(url);
            }
          }).catch(err => console.log('An error occurred', err));
        
       }
  render() {
    const { region } = this.props;
   // console.log(this.state.latlng);
 
    return (
      <View style={styles.container}>
      
        <MapView
          style={styles.map}
          region={{
            // latitude: 37.78825,
            // longitude: -122.4324,
            latitude:this.state.lat,
            longitude:this.state.long,
            latitudeDelta:this.state.latDelta,
            longitudeDelta:this.state.longDelta,
         
          }}
          onRegionChangeComplete={(data)=>{console.log(data),this.setState({lat:data.latitude,long:data.longitude,latDelta:data.latitudeDelta,longDelta:data.longitudeDelta})}}
          customMapStyle={customStyle}
          >
        
        {this.state.latlng.map((data,index) =>
            (
             
                <Marker
                style={{ zIndex: 1,}}
                key={index}
                onPress={() =>{this.setState({id:this.state.id != null ? null : data.id ,long:data.marker.longitude,lat:data.marker.latitude})}}
                coordinate={data.marker}>
                
                <View style={{justifyContent:'center',alignItems:'center'}} >

                   
                    <TouchableOpacity style={{borderWidth:3,borderRadius:50,height:38,width:38,justifyContent:'center',alignItems:'center',backgroundColor:'white',padding:1}} >
                     
                      <Text style={{fontSize:8,color:'red',fontWeight:'bold'}}>{data.name}</Text>
                      <Text style={{fontSize:8,fontWeight:'bold'}}>{data.id}</Text>
                    </TouchableOpacity>
                    <View style={{borderRightWidth:Platform.OS == 'ios' ? 1.5 : 1,height:12,borderColor:Platform.OS == 'android' ? '#ffffff' : null}} />
                    <Image  source={require('../../images/products/marker.png')} style={{tintColor:Platform.OS == 'android' ? '#ffffff' : null,height:16,width:45}} />
                </View>
            </Marker>
            
          )
        )}
                </MapView>
                {this.state.latlng.map((data,index) => 

                this.state.id == data.id ? 
            
                  <View>
                    
                    <View style={{backgroundColor:'black',width:250,height:180,borderRadius:3,marginTop:'10%'}} >
                
                        <View style={{padding:10,alignItems:'center',flexDirection:'row'}}>
                            <View><Image source={require('../../images/products/addDriver.png')} style={{height:20,width:20}}/></View>
                            <View style={{justifyContent:'space-between',flexDirection:'row',marginLeft:10,width:200,padding:10}}>
                               <Text style={{color:'white',fontSize:10}}> {data.name} </Text>
                               <Text style={{color:'white',fontSize:10}}> CODE {data.id} </Text>
                            </View>
                        </View>    
                     
                        <View >
                         
                            <View style={{flexDirection:'row',justifyContent:'space-between',paddingLeft:10,paddingRight:10,padding:1}}>
                                <Text style={{color:'white',fontSize:10}}>Addresss  </Text>
                                <View style={{height:25,width:'65%',alignItems:'flex-start'}}><Text style={{color:'white',fontSize:10}}>{data.address}</Text></View>
                            </View>
                            <View style={{marginTop:10,flexDirection:'row',justifyContent:'space-between',paddingLeft:10,paddingRight:10,padding:1}}>
                                <Text style={{color:'white',fontSize:10}}>Phone Number  </Text>
                                <View style={{height:25,width:'65%',alignItems:'flex-start'}}><Text style={{color:'white',fontSize:10}}>{data.phone}</Text></View>
                            </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',padding:5,marginTop:-10}}>
                            <View style={{alignItems:'center',width:'50%'}}>
                                <Icon name="phone-in-talk" type="MaterialIcons" style={{color:'white',fontSize:22}} />
                                <TouchableOpacity style={{padding:10,backgroundColor:'white',borderRadius:3,marginTop:3}} onPress={()=>this.sendSMS('tel:'+data.phone)} >
                                    <Text style={{fontSize:8,fontWeight:'bold'}}>MAKE CALL TO DRIVER</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{alignItems:'center',width:'50%'}}>
                                <Icon name="ios-mail-outline" style={{color:'white',fontSize:22}} />
                                <TouchableOpacity style={{padding:10,backgroundColor:'white',borderRadius:3,marginTop:3}} onPress={()=>this.sendSMS('sms:'+data.phone)} >
                                    <Text style={{fontSize:8,fontWeight:'bold'}}>SEND SMS TO DRIVER</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        
                    </View>
                      <View style={{marginTop:-22,alignItems:'center'}}>
                        <Image source={require('../../images/products/triangleDown.png')} style={{height:40,width:35}} />
                      </View>
                  </View>: null )}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    height:Platform.OS == 'android' ? '100%' : '100%',
    width: '100%',
    flex: 1
  },
});

const customStyle = [
  {
    elementType: 'geometry',
    stylers: [
      {
        color: '#242f3e',
      },
    ],
  },
  {
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#746855',
      },
    ],
  },
  {
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#242f3e',
      },
    ],
  },
  {
    featureType: 'administrative.locality',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#d59563',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#d59563',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'geometry',
    stylers: [
      {
        color: '#263c3f',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#6b9a76',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [
      {
        color: '#38414e',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#212a37',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9ca5b3',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry',
    stylers: [
      {
        color: '#746855',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#1f2835',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#f3d19c',
      },
    ],
  },
  {
    featureType: 'transit',
    elementType: 'geometry',
    stylers: [
      {
        color: '#2f3948',
      },
    ],
  },
  {
    featureType: 'transit.station',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#d59563',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [
      {
        color: '#17263c',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#515c6d',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#17263c',
      },
    ],
  },
];