
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  Picker,
  FlatList,
  ActivityIndicator
} from 'react-native';
import { Icon } from 'native-base';
import Tab from '../../components/Tab';
import ImagePicker from 'react-native-image-crop-picker';
import Api from '../../components/api'

export default class TotalDeals extends Component {
    constructor(props){
      super(props);
      this.state={
        btn:false,
        customer:'',
        indicator: true,
      }
    }


    static navigationOptions = {
        headerTitle : <View style={{width:'80%'}}><Text style={{color:'red',textAlign:'center',fontSize:Platform.OS == 'android' ? 18 : 15}}>CUSTOMER INFORMATION</Text></View>
    }

    componentDidMount(){
      Api.post('app/api/getCustomerList')
      .then((resp)=>{
        //console.log('data',resp.user_list)
        this.setState({customer:resp.user_list,indicator:false})
      })
  }
  render() {
    return (
      <View style={styles.container}>
         {this.state.indicator == true ? <View style={{marginTop: '50%'}}><ActivityIndicator size="large" color="white" /></View> :
            <FlatList
                data={this.state.customer}
                renderItem={({item,index})=>
            <View style={{padding:8,width:'100%',flexDirection:'row',borderWidth:1,borderColor:'white',borderBottomWidth:0.5,justifyContent:'space-between'}}>
                <View style={{alignItems:'center',justifyContent:'center',marginLeft:10,flexDirection:'row'}}>
                    <Image source={item.photo_url?{uri:item.photo_url}:require('../../images/products/images.png')} style={{height:40,width:40,borderRadius:20}} />
                    <View style={{marginLeft:5}}>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}} >{item.user_name}</Text>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}}>{item.email}</Text>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}}>{item.phone_number}</Text>
                    </View>
                </View>
               
            </View>
             }
             keyExtractor={(item,index)=>item.user_id}
         />
            }
        {/* <Tab/> */}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:20,
    fontWeight:Platform.OS == 'android' ? '500' :'900'
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  inputView:{
    flexDirection:'row',
    width:'66%',
    height:35, 
    marginLeft:20,
    borderRadius:3,
    backgroundColor:'white'
  },
 
});