
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  Alert,
  ActivityIndicator
} from 'react-native';
import { Icon } from 'native-base';
import Tab from '../../components/Tab';
import { Actions } from 'react-native-router-flux';
import Api from '../../components/api';

export default class TotalDeals extends Component {
  constructor(props){
    super(props);
    this.state={
        userID:'',
        distinguish:'',
        privacy:'',
        termsCondition:'',
        indicator: false,
    }
}


    static navigationOptions = {
        headerTitle : <View style={{width:'80%'}}><Text style={{color:'red',textAlign:'center',fontSize:18}}>EDIT ABOUT US</Text></View>
    }

    setDistinguish(){
      this.setState({indicator:true})
      if(this.state.distinguish){
        var formData = new FormData()
        formData.append("distinguish_us", this.state.distinguish);
        Api.post('app/api/setDistinguishUs',formData)
        .then((resp)=>
                {
                  console.log('data : ',resp)
                    if(resp.message == 'Success.')
                    {
                      Alert.alert(
                        'Success',
                        'Add Distinguish Us Succsessfully !',
                      )
                      this.setState({indicator:false})
                      this.setState({distinguish:''})
                    }
               })
      }
      else{
        this.setState({indicator:false})
        Alert.alert(
          'Invalid',
          'Please fill the Distinguish Us!',
        )
      }
    }
    setPrivacy(){
      this.setState({indicator:true})
      if(this.state.privacy){
        var formData = new FormData()
        formData.append("privacy_policy", this.state.privacy);
        Api.post('app/api/setPrivacyPolicy',formData)
        .then((resp)=>
                {
                  console.log('data : ',resp)
                    if(resp.message == 'Success.')
                    {
                      this.setState({indicator:false})
                      Alert.alert(
                        'Success',
                        'Add Privacy Policy Succsessfully !',
                      )
                      this.setState({privacy:''})
                    }
               })
      }
      else{
        this.setState({indicator:false})
        Alert.alert(
          'Invalid',
          'Please fill the Privacy Policy!',
        )
      }
    }
    setTermsCondition(){
      this.setState({indicator:true})
      if(this.state.termsCondition){
        var formData = new FormData()
        formData.append("terms_conditions", this.state.termsCondition);
        Api.post('app/api/setTermsConditions',formData)
        .then((resp)=>
                {
                  console.log('data : ',resp)
                    if(resp.message == 'Success.')
                    {
                      this.setState({indicator:false})
                      Alert.alert(
                        'Success',
                        'Add Terms Conditions Succsessfully !',
                      )
                      this.setState({termsCondition:''})
                    }
               })
      }
      else{
        this.setState({indicator:false})
        Alert.alert(
          'Invalid',
          'Please fill the Terms Conditions!',
        )
      }
    }
  render() {
    return (
      <View style={styles.container}>
      <View style={{backgroundColor:'white',alignItems:'center',width:'100%', flex:1,marginTop:5}}>
      {this.state.indicator == true ? <View style={{marginTop: '50%'}}><ActivityIndicator size="large" color="red" /></View> :
      <ScrollView style={{width:'100%'}}>
          <View style={{justifyContent:'space-between',width:'100%',alignItems:'center',padding:20}}>
          <View style={styles.input}>
              <TextInput
              multiline={true}
              placeholder='Distinguish Us'
              underlineColorAndroid='transparent'
              onChangeText={(text)=> this.setState({distinguish:text})}
              value={this.state.distinguish}
              />
              </View>
              <TouchableOpacity style={styles.btn} onPress={() => this.setDistinguish()}>
                  <Text style={styles.btnText}>Distinguish Us</Text>
              </TouchableOpacity>
              <View style={styles.input}>
              <TextInput
              multiline={true}
              placeholder='Privacy Policy'
              underlineColorAndroid='transparent'
              onChangeText={(text)=> this.setState({privacy:text})}
              value={this.state.privacy}
              />
              </View>
              <TouchableOpacity style={styles.btn} onPress={() => this.setPrivacy()}>
                  <Text style={styles.btnText}>Privacy Policy</Text>
              </TouchableOpacity>
              <View style={styles.input}>
              <TextInput
              multiline={true}
              placeholder='Tearms and Conditions'
              underlineColorAndroid='transparent'
              onChangeText={(text)=> this.setState({termsCondition:text})}
              value={this.state.termsCondition}
              />
              </View>
              <TouchableOpacity style={styles.btn} onPress={() => this.setTermsCondition()}>
                  <Text style={styles.btnText}>Tearms and Conditions</Text>
              </TouchableOpacity>
          </View>
          </ScrollView>
      }
      </View>
    
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:15
  },
  btn:{
    marginTop:20,
    backgroundColor:'#ea0017',
    height:40,
    width:'90%',
    justifyContent:'center',
    borderRadius:5
  },
  btnText:{
    color:'white',
    fontSize:16,
    fontWeight:Platform.OS == 'android' ? '500' : '800',
    textAlign:'center'
  },
  input:{
      marginTop:20,
      borderWidth:1,
      borderColor:'grey',
      borderRadius:5,
      width:'90%'
  }
  
  
 
});
