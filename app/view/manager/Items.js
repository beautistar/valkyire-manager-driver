
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  Modal,
  FlatList,
  ActivityIndicator,
  Model,
  RefreshControl
} from 'react-native';
import {  Header, Left, Body, Right, Button, Title, Icon} from 'native-base';

import Tab from '../../components/Tab';
import { Actions } from 'react-native-router-flux';
import Api from '../../components/api';
// import EditItem from '../manager/EditItems';
// import AddItem from '../manager/AddItems';

export default class Items extends Component {
    constructor(props){
        super(props);
        this.state={
            itemList:'',
           indicator:true,
           item:'',
           add:false,
          
        }
       
    }
   
    componentDidMount(){
    
      list=[];
      c=0;
      j=0;
      let catogory=['clothes','carpets','blanket','curtains','shoes','cover']
      var formData = new FormData()
          formData.append('service_type',catogory[0]);
          Api.post('app/api/getServiceList',formData)
          .then((resp)=>{
            console.log(resp)
            c++
            if(resp.message == 'Success.')
            {
              for(i=0;i<resp.service_list.length;i++){
                list[j]=resp.service_list[i]
                j++
               }  
              }            
              formData.append('service_type',catogory[1]);
              Api.post('app/api/getServiceList',formData)
              .then((resp)=>{
                if(resp.message == 'Success.')
                {
                  for(i=0;i<resp.service_list.length;i++){
                    list[j]=resp.service_list[i]
                    j++
                   }  
                  }            
                  formData.append('service_type',catogory[2]);
                  Api.post('app/api/getServiceList',formData)
                  .then((resp)=>{
                    if(resp.message == 'Success.')
                    {
                      for(i=0;i<resp.service_list.length;i++){
                        list[j]=resp.service_list[i]
                        j++
                       }  
                      }            
                      formData.append('service_type',catogory[3]);
                      Api.post('app/api/getServiceList',formData)
                      .then((resp)=>{
                        if(resp.message == 'Success.')
                        {
                          for(i=0;i<resp.service_list.length;i++){
                            list[j]=resp.service_list[i]
                            j++
                           }
                          }
                          formData.append('service_type',catogory[4]);
                          Api.post('app/api/getServiceList',formData)
                          .then((resp)=>{
                            if(resp.message == 'Success.')
                            {
                              for(i=0;i<resp.service_list.length;i++){
                                list[j]=resp.service_list[i]
                                j++
                              }
                            }
                            formData.append('service_type',catogory[5]);
                              Api.post('app/api/getServiceList',formData)
                              .then((resp)=>{
                                if(resp.message == 'Success.')
                                {
                                  for(i=0;i<resp.service_list.length;i++){
                                    list[j]=resp.service_list[i]
                                    j++
                                  }  
                                 
                                }
                                this.setState({itemList:list,indicator:false})
                                console.log("list :",list)

                              })
                              
                            
                          });  
                        
                      });
                    
                  });
                
              });
            
          });
        
    }
    static navigationOptions = ({navigation})=>({
        
        headerTitle : <View style={{width:'85%'}}><Text style={{color:'red',textAlign:'center',fontSize:18}}>ITEM LIST</Text></View>,
        headerRight : <TouchableOpacity style={{marginRight:5,backgroundColor:'red',padding:3,width:60,borderRadius:2,justifyContent:'center',alignItems:'center'}} onPress={() => navigation.navigate('AddItem')} ><Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:13}}>ADD</Text></TouchableOpacity>

    })
    
    

  render() {
    return (
      <View style={styles.container}>
        
        <ScrollView style={{width:'100%'}}>
        {this.state.indicator == true ? <View style={{marginTop: '50%'}}><ActivityIndicator size="large" color="white" /></View> :
          <View>
          <FlatList
            data={this.state.itemList}
            renderItem={({item})=>                         
            <View style={{padding:10,width:'100%',flexDirection:'row',borderWidth:1,borderColor:'white',borderBottomWidth:0.5,justifyContent:'space-between'}}>
                            <View style={{alignItems:'center',justifyContent:'center',marginLeft:10,flexDirection:'row'}}>
                                <Image source={{uri:item.photo_url}} style={{height:41,width:32}} />
                                <View style={{marginLeft:20}}>
                                     <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:15}} >{item.service_name}</Text>
                                      <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:15}}>{item.price} JD</Text>
                                </View>
                            </View>
                            <View style={{justifyContent:'center'}}>
                                 <TouchableOpacity onPress={() => this.props.navigation.navigate('EditItem',{item:item})} style={{backgroundColor:'white',padding:3,width:60,borderRadius:2,justifyContent:'center',alignItems:'center'}} ><Text style={{color:'red',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:13}}>Edit</Text></TouchableOpacity>
                            </View>
                </View>            
            }
           keyExtractor={(item,index)=>item.service_id}
            />
            
            </View>
        }
        </ScrollView>
        {/* <Tab/> */}
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:20,
    fontWeight:Platform.OS == 'android' ? '500' :'900'
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  inputView:{
    flexDirection:'row',
    width:'66%',
    height:35, 
    marginLeft:20,
    borderRadius:3,
    backgroundColor:'white'
  },
  header: {
    backgroundColor:'white',
    width:'100%',
    justifyContent: 'center',
  },
});
