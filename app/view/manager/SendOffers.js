
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  Modal,
  FlatList,
  ActivityIndicator,
  ToastAndroid,
    Alert
} from 'react-native';
import { Icon } from 'native-base';
import Tab from '../../components/Tab';
import Api from '../../components/api'

export default class SendOffers extends Component {
    constructor(props){
        super(props);
        this.state={
            offer : false,
            code : false,
            all : false,
            discount:false,
            customer:'',
            indicator: true,
            userid:'',
            offcode:'',
            dis:'',
            about_offer:'',
        }
    }


    static navigationOptions = {
        headerTitle : <View style={{width:'80%'}}><Text style={{color:'red',textAlign:'center',fontSize:18}}>SEND OFFERS</Text></View>
    }

    openModal(value,done){
        if(this.state.dis){
            this.setState({offer:value})
        }
        // else{
        //     this.setState({discount:false})
        //     Alert.alert(
        //         'Validation',
        //         'Please  Enter Discount ...!',
        //     )
        // }
       
 //console.log('discount',this.state.dis)
        if(done){

      
        if(this.state.about_offer!=''){
            let {userid,offcode,about_offer,dis}=this.state;
           
               // console.log(userid,":",offcode,":",about_offer)
                var formData = new FormData()
                formData.append("user_id", userid);
                formData.append("off_code", offcode);
                formData.append("about_offer", about_offer);
                formData.append("discount", dis);
                Api.post('app/api/sendOffCode',formData)
                .then((resp)=>
                {
                    if(resp.message == 'Success.')
                    {
                        Alert.alert(
                            'Success',
                            'Offer Code Send Successfully ...!',
                        )
                        //console.log('Send Offer Code Sucessfully..!')
                    }
                })
                    this.setState({userid:'',offcode:'',about_offer:''})
        }
        // else{
        //     Alert.alert(
        //         'Validation',
        //         'Please  Enter About Offer...!',
        //     )
        // }
        
    }
    }
    openModal1(value){
            this.setState({code:value})
    }
    openModal2(value){
        if(this.state.offcode){
            this.setState({discount:value})
        }
        // else
        // {
        //     this.setState({code:false})
        //     Alert.alert(
        //         'Validation',
        //         'Please  Enter Code ...!',
        //     )
        // }
        
      
    }
    componentDidMount(){
        Api.post('app/api/getCustomerList')
        .then((resp)=>{
          //console.log('data',resp.user_list)
          this.setState({customer:resp.user_list,indicator:false})
        })
    }
  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{width:'100%'}}>
            {/* <View style={{padding:5,width:'100%',justifyContent:'space-between',flexDirection:'row'}}>
               <TouchableOpacity style={{padding:5,width:'30%',justifyContent:'center',alignItems:'center'}} >
                   <Image source={require('../../images/products/createOffer.png')} style={{height:20,width:20,marginLeft:5}} /> 
                   <Text style={{color:'white',marginLeft:5,fontSize:10,fontWeight:Platform.OS == 'android'?'500' : '700'}} >CREATE OFFER</Text>
               </TouchableOpacity>
               <TouchableOpacity style={{padding:5,width:'30%',justifyContent:'center',alignItems:'center'}}  >
               <Image source={require('../../images/products/makeCode.png')} style={{height:23,width:20,marginLeft:5}} /> 
                   <Text style={{color:'white',marginLeft:5,fontSize:10,fontWeight:Platform.OS == 'android'?'500' : '700'}} >MAKE CODE</Text>
               </TouchableOpacity>
               <TouchableOpacity style={{padding:5,width:'30%',justifyContent:'center',alignItems:'center'}} >
               <Image source={require('../../images/products/sendToAll.png')} style={{height:20,width:20,marginLeft:5}} /> 
                   <Text style={{color:'white',marginLeft:5,fontSize:10,fontWeight:Platform.OS == 'android'?'500' : '700'}} >SEND TO ALL</Text>
               </TouchableOpacity>
            </View> */}
           
           {this.state.indicator == true ? <View style={{marginTop: '50%'}}><ActivityIndicator size="large" color="white" /></View> :
            <FlatList
                data={this.state.customer}
                renderItem={({item,index})=>
            <View style={{padding:8,width:'100%',flexDirection:'row',borderWidth:1,borderColor:'white',borderBottomWidth:0.5,justifyContent:'space-between'}}>
                <View style={{alignItems:'center',justifyContent:'center',marginLeft:10,flexDirection:'row'}}>
                    <Image source={item.photo_url?{uri:item.photo_url}:require('../../images/products/images.png')} style={{height:40,width:40,borderRadius:20}} />
                    <View style={{marginLeft:5}}>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}} >{item.user_name}</Text>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}}>{item.email}</Text>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}}>{item.phone_number}</Text>
                    </View>
                </View>
                <View style={{justifyContent:'center'}}>
                    <TouchableOpacity style={{backgroundColor:'white',padding:3,width:80,borderRadius:2,justifyContent:'center',alignItems:'center'}} onPress={() =>{this.setState({userid:item.user_id}) ,this.openModal1(!this.state.code)}} ><Text style={{color:'red',marginLeft:5,fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:10}}>SEND OFFER</Text></TouchableOpacity>
                </View>
            </View>
             }
             keyExtractor={(item,index)=>item.user_id}
         />
        
            }  
            <View>
                    <Modal
                    transparent={true}
                    visible={this.state.offer}
                    onRequestClose={() => this.setState({offer:false}) }
                    >
                        <View style={{backgroundColor:'black',width:'70%',top:'18%',position:'absolute',marginLeft:'12%',padding:5}} >
                         <Text style={{color:'white',fontSize:10,padding:10}} >Write About Offer</Text>
                         <TextInput 
                            multiline={true}
                            numberOfLines={5}
                            underlineColorAndroid="grey"
                            style={{color:'white'}}
                            onChangeText={(aboutoffer)=>this.setState({about_offer:aboutoffer})}
                         />
                         <TouchableOpacity style={{backgroundColor:'white',width:70,alignSelf:'flex-end',padding:2,borderRadius:2,justifyContent:'center',alignItems:'center',marginRight:5}} onPress={()=> this.openModal(!this.state.offer,true)}>
                            <Text style={{fontSize:12,fontWeight:'500'}}>Done</Text>
                         </TouchableOpacity>
                        </View>
                    </Modal>
            </View>   
            <View>
                    <Modal
                    transparent={true}
                    visible={this.state.code}
                    onRequestClose={() => this.setState({code:false}) }
                    >
                        <View style={{backgroundColor:'black',top:'18%',position:'absolute',marginLeft:'20%',padding:10,flexDirection:'row',alignItems:'center',paddingTop:20,paddingBottom:20}} >
                        <Text style={{color:'white',fontSize:10,fontWeight:'500'}} >Make Code</Text>
                        <View style={{ flexDirection:'row',backgroundColor:'white',borderRadius:3,flexDirection:'row',height:18,marginLeft:5,width:130}}> 
                         <TextInput style={{width:90,marginTop:-10,height:40,fontSize:10}} underlineColorAndroid="transparent"
                         onChangeText={(code)=>this.setState({offcode:code})}
                          />
                         <View style={{flex:1,alignItems:'flex-end'}}>
                         <TouchableOpacity style={{height:18,width:40, backgroundColor:'red',alignItems:'center',borderTopRightRadius:3,borderBottomRightRadius:3}} onPress={()=> {this.openModal2(!this.state.discount),this.openModal1(!this.state.code)}}>
                            <Text style={{fontSize:10,fontWeight:'500',color:'white'}}>SEND</Text>
                         </TouchableOpacity></View>
                        </View>
                        </View>
                    </Modal>
            </View>   
            <View>
                    <Modal
                    transparent={true}
                    visible={this.state.discount}
                    onRequestClose={() => this.setState({all:false}) }
                    >
                        <View style={{backgroundColor:'black',top:'18%',position:'absolute',marginLeft:'20%',padding:10,flexDirection:'row',alignItems:'center',paddingTop:20,paddingBottom:20}} >
                        <Text style={{color:'white',fontSize:10,fontWeight:'500'}} >Discount</Text>
                        <View style={{ flexDirection:'row',backgroundColor:'white',borderRadius:3,flexDirection:'row',height:18,marginLeft:5,width:130}}> 
                        <TextInput style={{width:90,marginTop:-10,height:40,fontSize:10}}
                          underlineColorAndroid="transparent"
                          onChangeText={(text)=>this.setState({dis:text})} />
                         <View style={{flex:1,alignItems:'flex-end'}}>
                         <TouchableOpacity style={{height:18,width:40, backgroundColor:'red',alignItems:'center',borderTopRightRadius:3,borderBottomRightRadius:3}} onPress={()=> {this.openModal(!this.state.offer),this.openModal2(!this.state.discount)}}>
                            <Text style={{fontSize:10,fontWeight:'500',color:'white'}}>SEND</Text>
                         </TouchableOpacity></View>
                        </View>
                        </View>
                    </Modal>
            </View>   
        </ScrollView>
        {/* <Tab/> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:20,
    fontWeight:Platform.OS == 'android' ? '500' :'900'
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  inputView:{
    flexDirection:'row',
    width:'66%',
    height:35, 
    marginLeft:20,
    borderRadius:3,
    backgroundColor:'white'
  },
 
});
