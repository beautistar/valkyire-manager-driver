
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  FlatList,
  ActivityIndicator,
  Alert
} from 'react-native';
import { Icon } from 'native-base';
import Tab from '../../components/Tab';
import Api from '../../components/api';
import { Actions } from 'react-native-router-flux';

export default class EditAndRemoveDriver extends Component {

    static navigationOptions = {
        headerTitle : <View style={{width:'90%'}}><Text style={{color:'red',textAlign:'center',fontSize:18}}>REMOVE DRIVERS</Text></View>
    }
    constructor(props){
        super(props);
        this.state={
            data:'',
            indicator: true,
            delete:false,
           
        }
    }
    componentDidMount(){
      driver=[];
      cust=[];
      list=[]
      i=0,j=0
        Api.post('app/api/getDriverCustomerList')
        .then((resp)=>{
         // console.log('data',resp)
         resp.user_list.forEach(item => {
           if(item.role=='driver'){
             driver[i]=item
             i++;
           }
           else
           {
              cust[j]=item
              j++
           }
         });
         i=0
        cust.forEach(cust => {
           list[i]=cust
           i++
         });
         driver.forEach(driver => {
          list[i]=driver
          i++
        });

          this.setState({data:list,indicator:false})
        })
    }
    removeDriver(id,name,role){
      type=''
      if(role=='driver'){
        type='Driver'
      }
      else{
        type='Customer'
      }
      Alert.alert(
        'Delete',
        ' Are you sure want to delete this '+type+' '+name+' ?',
        [
          {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'Yes', onPress: () => {

            this.setState({indicator:true})

            var formData = new FormData()
            formData.append("user_id",id);
            Api.post('app/api/removeDriver',formData)
            .then((resp)=>{
             // console.log('data',resp)

              if(resp.message == 'Success.')
              {
                driver=[];
                cust=[];
                list=[]
                i=0,j=0
                  Api.post('app/api/getDriverCustomerList')
                  .then((resp)=>{
                  // console.log('data',resp)
                  resp.user_list.forEach(item => {
                    if(item.role=='driver'){
                      driver[i]=item
                      i++;
                    }
                    else
                    {
                        cust[j]=item
                        j++
                    }
                  });
                  i=0
                  cust.forEach(cust => {
                    list[i]=cust
                    i++
                  });
                  driver.forEach(driver => {
                    list[i]=driver
                    i++
                  });
          
                    this.setState({data:list,indicator:false})
                  })
              }
           
            })

            
          }},
        ],
        { cancelable: false }
      )
      // Api.post('app/api/getDriverCustomerList')
      // .then((resp)=>{
      //   console.log('data',resp.user_list)
      //   this.setState({data:resp.user_list,indicator:false})
      // })
    }
    delete(){
        this.setState({delete:!this.state.delete})
       
    }
  render() {
   // console.log('delete',this.state.delete);
    return (
      <View style={styles.container}>
        <ScrollView style={{width:'100%'}}>
            <View style={{padding:15,width:'100%',justifyContent:'space-between',flexDirection:'row'}}>
               <TouchableOpacity style={{backgroundColor:'white',padding:5,width:'35%',flexDirection:'row',borderRadius:3}} onPress={()=>Actions.addDriver({EditAndRemoveDriver:true})}>
                   <Image source={require('../../images/products/driver.png')} style={{height:20,width:21,marginLeft:5}} /> 
                   <Text style={{color:'red',marginLeft:5,fontSize:12,fontWeight:Platform.OS == 'android'?'500' : '700'}} >Add Driver</Text>
               </TouchableOpacity>
               <TouchableOpacity style={{backgroundColor:'white',padding:5,width:'35%',flexDirection:'row',borderRadius:3}}  onPress={()=>this.delete()}>
               <Image source={require('../../images/products/delete.png')} style={{height:16,width:13,marginLeft:5}} /> 
                   <Text style={{color:'red',marginLeft:5,fontSize:12,fontWeight:Platform.OS == 'android'?'500' : '700'}} >Delete Driver</Text>
               </TouchableOpacity>
            </View>
           
            {this.state.indicator == true ? <View style={{marginTop: '50%'}}><ActivityIndicator size="large" color="white" /></View> :
            <FlatList
                extraData={this.state}
                data={this.state.data}
                renderItem={({item,index})=>
            <View style={{padding:8,width:'100%',flexDirection:'row',borderWidth:1,borderBottomWidth:0.5,borderColor:'white',justifyContent:'space-between'}}>
                <View style={{marginLeft:10}}>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}} >{item.user_name}</Text>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}} >{item.role}</Text>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}}>{item.email}</Text>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}}>{item.phone_number}</Text>
                </View>
                <View style={{justifyContent:'center',flexDirection:'row'}}>
                    <Image source={item.photo_url?{uri:item.photo_url}:require('../../images/products/images.png')} style={{height:50,width:50,borderRadius:25}} />
                    {this.state.delete?<TouchableOpacity onPress={()=>this.removeDriver(item.user_id,item.user_name,item.role)}><Icon name="ios-trash-outline" style={{padding:3,marginTop:5,fontSize:Platform.OS == 'android' ? 30 : 30,color:'white'}}/></TouchableOpacity>:null}
                    {/* <Text style={{color:'white',marginLeft:5,fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}}>Edit</Text> */}
                </View>
            </View>
                }
                keyExtractor={(item,index)=>item.user_id}
            />
            }
        </ScrollView>
        {/* <Tab/> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:20,
    fontWeight:Platform.OS == 'android' ? '500' :'900'
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  inputView:{
    flexDirection:'row',
    width:'66%',
    height:35, 
    marginLeft:20,
    borderRadius:3,
    backgroundColor:'white'
  },
 
});
