
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  Picker,
  FlatList,
  ActivityIndicator
} from 'react-native';
import { Icon } from 'native-base';
import Tab from '../../components/Tab';
import ImagePicker from 'react-native-image-crop-picker';
import Api from '../../components/api'

export default class TotalDeals extends Component {
    constructor(props){
      super(props);
      this.state={
        btn:false,
        order:'',
        indicator: true,
      }
    }


    static navigationOptions = {
        headerTitle : <View style={{width:'80%'}}><Text style={{color:'red',textAlign:'center',fontSize:18}}>TOTAL DEALS</Text></View>
    }
    componentDidMount(){
      Api.post('app/api/getOrderList')
      .then((resp)=>{
        //console.log('data',resp.order_list)
        if(resp.order_list != undefined)
        {
          this.setState({order:resp.order_list.reverse(),indicator:false})
        }
        else
        {
          this.setState({indicator:false})
        }
      })
  }

  render() {
    return (
      <View style={styles.container}>
         {this.state.indicator == true ? <View style={{marginTop: '50%'}}><ActivityIndicator size="large" color="white" /></View> :
            <FlatList
                data={this.state.order}
                renderItem={({item,index})=>
            <View style={{padding:8,width:'100%',borderWidth:1,borderColor:'white',borderBottomWidth:0.5,justifyContent:'space-between'}}>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}} >Customer Name : {item.customer_name}</Text>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}}>Ivoice Number : {item.invoice_number}</Text>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}}>Total Item : {item.total_itmes}</Text>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}} >Total Cost : {item.total_cost}</Text>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}}>Adress : {item.address}</Text>
                    <Text style={{color:'white',fontWeight:Platform.OS == 'android'?'500' : '700',fontSize:12}}>Order Date : {item.ordered_at}</Text>
                    </View>
             }
             keyExtractor={(item,index)=>item.order_id}
         />
            }
        {/* <Tab/> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:20,
    fontWeight:Platform.OS == 'android' ? '500' :'900'
  },
  
 
});
