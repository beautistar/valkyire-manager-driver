
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  StatusBar,
  ToastAndroid,
  ActivityIndicator,
  Alert,
  Modal
} from 'react-native';
import { Icon } from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
import Api from '../../components/api';
import { Actions } from 'react-native-router-flux';

export default class AddDriver extends Component {

    static navigationOptions ={
        headerTitle : <View style={{width:'80%',alignItems:'center',justifyContent:'center'}}><Text style={{color:'red',textAlign:'center',fontSize:18}}>ADD DRIVERS</Text></View>
    }
    constructor(props){
      super(props);
      this.state={
        email:'',
        password:'',
        fullname:'',
        mobile:'',
        address:'',
        path:'',
        indicator: false,
        modalVisible: false,
      }
    }
    setModalVisible(visible) {
      this.setState({ modalVisible: visible });
      //alert(visible)
    }
    Camera(){
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      }).then(response => {
        console.log(response)
      this.setState({path:response.path})
      this.setState({modalVisible:false})
      })
     // this.setModalVisible(!this.state.modalVisible);
     
      }
      Gallery(){
        ImagePicker.openPicker({
          width: 300,
          height: 400,
          cropping: true,
        }).then(response => {
          console.log('path',response.path)
          this.setState({path:response.path})
          this.setState({modalVisible:false})
        });
        //this.setModalVisible(!this.state.modalVisible);
      }
      AddDriver(){
        this.setState({indicator:true})
        let{email,password,fullname,mobile,address}=this.state;
        if(this.state.email && this.state.password && this.state.fullname && this.state.mobile && this.state.address && this.state.path){
        var formData = new FormData()
        formData.append("user_name",fullname);
        formData.append("email", email);
        formData.append("phone_number", mobile);
        formData.append("password", password);
        formData.append("address",address);
       
        Api.post('app/api/addDriver',formData)
        .then((resp)=>
                {
                  console.log('data : ',resp)
                    if(resp.message == 'Success.')
                    {
                      var formData = new FormData()
                      formData.append("user_id",resp.user_id);
                      formData.append("file",{uri: this.state.path, name: 'product.jpg', type: 'image/jpg'});

                      Api.post('app/api/uploadPhoto',formData)
                      .then(result => {console.log('result',result)
                          if(result.message == 'Success.')
                          {
                            this.setState({indicator:false})
                            this.props.EditAndRemoveDriver?
                            Actions.removeDriver()
                            :
                            Actions.pop()
                          }
                        })
                      
                    }
    
                 
            })
        }
        else{
          this.setState({indicator:false})
          Alert.alert(
            'validation',
            'Please fill all the details ...!',
        )
        }
       
      }
    render() {
    return (
      <View style={styles.container}>
            {Platform.OS == 'android' ? <StatusBar backgroundColor='#ea0017'  barStyle= "light-content"/> : 
                                        <StatusBar backgroundColor='#ea0017'  barStyle= "dark-content"/>} 
              {this.state.indicator == true ? <View style={{marginTop: '50%'}}><ActivityIndicator size="large" color="white" /></View> :
            <View style={{height:'95%'}}>
            <ScrollView >
            <View style={{marginTop:30,padding:10,width:'100%',justifyContent:'center',alignItems:'center'}}>
                <Image source={require('../../images/products/addDriver.png')} style={{height:35,width:35}} />
                <Text style={{color:'white',fontSize:20,fontWeight:Platform.OS == 'android' ? '400' : '700',marginTop:5}}>ADD DRIVERS</Text>
            </View>
            <View style={{padding:10,width:'100%',justifyContent:'center',alignItems:'center'}}>
                
                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}} >
                    <Icon name="ios-person-outline" style={{padding:3,fontSize:Platform.OS == 'android' ? 40 : 40,color:'white'}}/>
                    <View style={{flexDirection:'row',width:'65%',height:Platform.OS == 'ios' ? 35 : 38,marginLeft:Platform.OS == 'android' ? 10 : 15,borderRadius:3,backgroundColor:'white'}}>
                        <TextInput  
                            style={{width:200,fontSize:12,marginLeft:5}} 
                            underlineColorAndroid="transparent"
                            placeholder="Full Name"
                            onChangeText={(text) => this.setState({fullname:text})}
                            />
                    </View>
                </View>

                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',marginTop:10}} >
                    <Image source={require('../../images/products/mobile.png')} style={{height:32,width:29.5}}/>
                    <View style={{flexDirection:'row',width:'65%',height:Platform.OS == 'ios' ? 35 : 38,marginLeft:Platform.OS == 'android' ? 10 : 15,borderRadius:3,backgroundColor:'white'}}>
                        <TextInput  
                            style={{width:200,fontSize:12,marginLeft:5}} 
                            underlineColorAndroid="transparent"
                            placeholder="Mobile Number"
                            onChangeText={(text) => this.setState({mobile:text})}
                            />
                    </View>
                </View>
                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',marginTop:10}} >
                    <Icon name="ios-person-outline" style={{padding:3,fontSize:Platform.OS == 'android' ? 40 : 40,color:'white'}}/>
                    <View style={{flexDirection:'row',width:'65%',height:Platform.OS == 'ios' ? 35 : 38,marginLeft:Platform.OS == 'android' ? 10 : 15,borderRadius:3,backgroundColor:'white'}}>
                        <TextInput  
                            style={{width:200,fontSize:12,marginLeft:5}} 
                            underlineColorAndroid="transparent"
                            placeholder="Email"
                            onChangeText={(text) => this.setState({email:text})}
                            />
                    </View>
                </View>
            
                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',marginTop:10}} >
                <Icon name="ios-key-outline" style={{padding:3,marginRight:3,fontSize:Platform.OS == 'android' ? 35 : 40,color:'white'}}/>
                    <View style={{flexDirection:'row',width:'65%',height:Platform.OS == 'ios' ? 35 : 38,marginLeft:Platform.OS == 'android' ? 3 : 15,borderRadius:3,backgroundColor:'white'}}>
                        <TextInput  
                            style={{width:200,fontSize:12,marginLeft:5}} 
                            underlineColorAndroid="transparent"
                            placeholder="********"
                            secureTextEntry={true}
                            onChangeText={(text) => this.setState({password:text})} 
                            />
                    </View>
                </View>
                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',marginTop:10}} >
                <Icon name="ios-home-outline" style={{marginRight:3,padding:3,fontSize:Platform.OS == 'android' ? 35 : 40,color:'white'}}/>
                    <View style={{flexDirection:'row',width:'65%',height:Platform.OS == 'ios' ? 35 : 38,marginLeft:Platform.OS == 'android' ? 3 : 15,borderRadius:3,backgroundColor:'white'}}>
                        <TextInput  
                            style={{width:200,fontSize:12,marginLeft:5}} 
                            underlineColorAndroid="transparent"
                            placeholder="Address"
                            onChangeText={(text) => this.setState({address:text})} 
                            />
                    </View>
                </View>
            </View>
            
            <View style={{marginTop:20,marginBottom:'10%',padding:10,alignItems:'center',width:'72%',backgroundColor:'white',alignSelf:'center',borderRadius:3}} >
            <TouchableOpacity onPress={() =>  this.setModalVisible(!this.state.modalVisible)}  >
            {!this.state.path?
              <Image  source={require('../../images/products/camera.png')} style={{margin:20,height:70,width:70}} />
              :<Image
              style={{margin:20,width:70,height:70,borderRadius:5}}
              source={{uri:this.state.path}}
               />
            }
              </TouchableOpacity>    
            </View>
           
            </ScrollView>
            <Modal
           animationType="slide"
           transparent={true}
           visible={this.state.modalVisible}
           onRequestClose={() => {this.setModalVisible(!this.state.modalVisible);}}>
           <View style={styles.modalview}>
             <View style={styles.modal}>
             <View style={{paddingLeft:'5%',paddingTop:'1%'}}>
                   <Text style={{fontSize:15,padding:5,color:'grey'}}>Select From</Text></View>
                <TouchableOpacity onPress={()=>this.Camera()} style={styles.modalbtn}><Icon name='camera' style={{color:'#2c8ef4',paddingRight:'2%'}}/><Text style={styles.modeltxt}>Camera</Text></TouchableOpacity>
                <TouchableOpacity onPress={()=>this. Gallery()} style={styles.modalbtn}><Icon name='md-images' style={{color:'#f42ced',paddingRight:'2%'}}/><Text style={styles.modeltxt}>Gallery</Text></TouchableOpacity>
               <TouchableOpacity style={styles.modalbtn}
                 onPress={()=>{this.setModalVisible(!this.state.modalVisible);}}>
                 <Icon name='close' style={{color:'#25de5b',paddingLeft:5,paddingRight:'2%'}}/><Text style={styles.modeltxt}>Cancel</Text>
               </TouchableOpacity>
             </View>
           </View>
         </Modal>
        </View>
              }
      
        <View style={{ bottom:0, position:'absolute', width: '100%',backgroundColor:'white',padding:12,justifyContent:'center',alignItems:'center'}}>
          <TouchableOpacity style={{width:'85%',justifyContent:'center',alignItems:'center'}} onPress={()=>this.AddDriver()} >
            <Text style={{fontSize:25,color:'black',fontWeight:Platform.OS == 'android' ? '500' : '800'}}>PROCEED</Text>
          </TouchableOpacity>
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:20,
    fontWeight:Platform.OS == 'android' ? '500' :'900'
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  inputView:{
    marginTop:10,
    flexDirection:'row',
    width:'66%',
    height:35, 
    marginLeft:20,
    borderRadius:3,
    backgroundColor:'white'
  },
  modeltxt:{
    fontSize:20,
    padding:'3%',
    paddingRight:'6%'
   },
   modalbtn:{
    paddingHorizontal:'2%',
     flexDirection:'row',
     alignItems:'center',
     paddingLeft:'5%'
   },
   modal: {
    height: '30%',
    width: '100%',
    backgroundColor:'white',
    borderRadius: 7,
    elevation:100,
   },
   modalview: {
    flex: 1,
    alignItems:'center',
    justifyContent:'flex-end',
   },
 
});
