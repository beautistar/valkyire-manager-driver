
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  Picker,
  Modal,
  Alert,
} from 'react-native';
import { Icon } from 'native-base';
import Tab from '../../components/Tab';
import ImagePicker from 'react-native-image-crop-picker';
import Api from '../../components/api';
import { Actions } from 'react-native-router-flux';


export default class AddItems extends Component {
    constructor(props){
      super(props);
      this.state={
        category:'Select',
        btn:false,
        btn2:false,
        itemName:'',
        itemType:'Select',
        itemPrice:'',
        path:'',
        modalVisible: false,
      }
    }

    static headerMode= 'float'
    static navigationOptions = {
        headerTitle : <View style={{width:'80%'}}><Text style={{color:'red',textAlign:'center',fontSize:18}}>ADD ITEMS</Text></View>
    }
    setModalVisible(visible) {
      this.setState({ modalVisible: visible });
      //alert(visible)
    }

    Camera(){
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      }).then(response => {
        console.log(response)
      this.setState({path:response.path})
      this.setState({modalVisible:false})
      })
     // this.setModalVisible(!this.state.modalVisible);
      }
      Gallery(){
        ImagePicker.openPicker({
          width: 300,
          height: 400,
          cropping: true,
        }).then(response => {
          this.setState({path:response.path})
          this.setState({modalVisible:false})
        });
        //this.setModalVisible(!this.state.modalVisible);
      }
      addItems(){
        let {itemName,itemType,itemPrice,category,path}=this.state;
        console.log('item: ',itemName,itemType,itemPrice,category,path)
        if(itemName && itemType != 'Select' && itemPrice && category != 'Select' && path){
              var formData = new FormData()
            
              formData.append("service_type",itemType);
              formData.append("category", category);
              formData.append("service_name", itemName);
              formData.append("price", itemPrice);
              formData.append("file",{uri: path, name: 'product.jpg', type: 'image/jpg'});
      
              Api.post('app/api/addServiceItem',formData)
              .then(result => {console.log('result',result)
                  if(result.message == 'Success.')
                  {
                    this.props.navigation.navigate('Items')
                  }
                })
        }
        else{
          this.setState({indicator:false})
          Alert.alert(
            'validation',
            'Please fill all the details ...!',
        )
        }
        
      }
    _category(type)
    {
     
        return(
           
          type == "Clothes" ?

          <Picker
          style={{height:28}}
          selectedValue={this.state.category}
          onValueChange={(itemValue, itemIndex) => this.setState({category: itemValue,btn:false})}>
          <Picker.Item label="Select" value="Select" />
          <Picker.Item label="Men" value="Men" />
          <Picker.Item label="Women" value="Women" />
          <Picker.Item label="kids" value="Kids" /> 
          </Picker>
         : type == "Carpets" ? 
         <Picker
         style={{height:28}}
         selectedValue={this.state.category}
         onValueChange={(itemValue, itemIndex) => this.setState({category: itemValue,btn:false})}>
         <Picker.Item label="Carpets" value="Carpets" />
         </Picker>
         
         : type == "Curtains" ? 
         <Picker
         style={{height:28}}
         selectedValue={this.state.category}
         onValueChange={(itemValue, itemIndex) => this.setState({category: itemValue,btn:false})}>
         <Picker.Item label="Curtains" value="Curtains" />
         </Picker>

        : type == "Shoes" ? 
        <Picker
        style={{height:28}}
        selectedValue={this.state.category}
        onValueChange={(itemValue, itemIndex) => this.setState({category: itemValue,btn:false})}>
        <Picker.Item label="Shoes" value="Shoes" />
        </Picker>

        : type == "Cover" ? 
        <Picker
        style={{height:28}}
        selectedValue={this.state.category}
        onValueChange={(itemValue, itemIndex) => this.setState({category: itemValue,btn:false})}>
        <Picker.Item label="Cover" value="Cover" />
        </Picker>

        : type == "Blanket" ? 
        <Picker
        style={{height:28}}
        selectedValue={this.state.category}
        onValueChange={(itemValue, itemIndex) => this.setState({category: itemValue,btn:false})}>
        <Picker.Item label="Blanket" value="Blanket" />
        </Picker>

        :
        <Picker
        style={{height:28}}
        selectedValue={this.state.category}
        onValueChange={(itemValue, itemIndex) => this.setState({category: itemValue,btn:false})}>
        <Picker.Item label="Select" value="Select" />
        <Picker.Item label="Men" value="Men" />
        <Picker.Item label="Women" value="Women" />
        <Picker.Item label="kids" value="Kids" /> 
        <Picker.Item label="Carpets" value="Carpets" />
        <Picker.Item label="Curtains" value="Curtains" />
        <Picker.Item label="Shoes" value="Shoes" />
        <Picker.Item label="Cover" value="Cover" />
        <Picker.Item label="Blanket" value="Blanket" />
        </Picker>
        );
      
    }


  render() {
    return (
      <View style={styles.container}>
      <View style={{backgroundColor:'white',height:55,justifyContent:'center',paddingTop:Platform.OS=='ios'?15:0}}>
          <View style={{flexDirection:'row'}}>
         <TouchableOpacity style={{width:'15%'}} onPress={()=>this.props.navigation.navigate('Items')}>
                <Icon name='md-arrow-back' style={{marginLeft:15,color:'black',fontSize:25}} />
         </TouchableOpacity>
          <Text style={{color:'red',fontSize:20,marginLeft:Platform.OS=='ios'?'25%':'18%'}}>Add Item</Text>
          </View>
         </View>
        <ScrollView>
            <View style={{padding:5,marginBottom:'15%'}}>
                
                <View style={{margin:10}}>
                    <Text style={{marginLeft:27.5,color:'white',marginBottom:5,fontSize:13}} >ITEM IMAGE</Text>
                    <View style={this.state.path?{marginLeft:Platform.OS == 'android' ? 28 : 29.5,width:'30%',borderRadius:3}
                    :{marginLeft:Platform.OS == 'android' ? 28 : 29.5,padding:10,width:'30%',justifyContent:'center',alignItems:'center',backgroundColor:'white',borderRadius:5}} >
                    
                      <TouchableOpacity onPress={() =>  this.setModalVisible(!this.state.modalVisible)} >
                        {!this.state.path?
                         <Image  source={require('../../images/products/camera.png')} style={{height:35,width:35}} />
                        :<Image
                        style={{width:80,height:60,borderRadius:5}}
                        source={{uri:this.state.path}}
                      />
                    
           }
                      </TouchableOpacity>    
                    </View>
                </View>
                <View style={{margin:10}} >
                    <Text style={{marginLeft:27.5,color:'white',marginBottom:5,fontSize:13}} >SELECT SERVICE TYPE</Text>
                    <View style={{backgroundColor:'white',width:'80%',height:Platform.OS == 'ios' ? 25 : 28,marginLeft:Platform.OS == 'android' ? 28 : 29.5,borderRadius:3}}> 
                    {Platform.OS == 'android' ? 
                     <Picker
                          style={{height:28}}
                          selectedValue={this.state.itemType}
                          onValueChange={(itemValue, itemIndex) => this.setState({itemType: itemValue})}>
                          <Picker.Item label="Select" value="Select" />
                          <Picker.Item label="Clothes" value="Clothes" />
                          <Picker.Item label="Carpets" value="Carpets" />
                          <Picker.Item label="Curtains" value="Curtains" />
                          <Picker.Item label="Shoes" value="Shoes" />
                          <Picker.Item label="Cover" value="Cover" />
                          <Picker.Item label="Blanket" value="Blanket" />
                    </Picker> : 
                    <TouchableOpacity onPress={() => this.setState({btn2:true})}> 
                        <Text style={{margin:5}}>{this.state.itemType}</Text>
                    </TouchableOpacity> }
                    </View> 
                </View>
                <View style={{margin:10}}>
                    <Text style={{marginLeft:27.5,color:'white',marginBottom:5,fontSize:13}} >ITEM NAME</Text>
                    <View style={{flexDirection:'row',width:'80%',height:25,marginLeft:Platform.OS == 'android' ? 28 : 29.5,borderRadius:3,backgroundColor:'white',alignItems:'center'}}>
                        <TextInput  
                            style={{width:200,height:40,marginLeft:5}} 
                            onChangeText={(name)=>this.setState({itemName:name})}
                            underlineColorAndroid="transparent"
                            />
                    </View>
                </View>
                {/* <View style={{margin:10}}>
                    <Text style={{marginLeft:27.5,color:'white',marginBottom:5,fontSize:13}} >ITEM TYPE</Text>
                    <View style={{flexDirection:'row',width:'80%',height:25 ,marginLeft:Platform.OS == 'android' ? 28 : 29.5,borderRadius:3,backgroundColor:'white',alignItems:'center'}}>
                        <TextInput  
                            style={{width:200,height:40,marginLeft:5}} 
                            onChangeText={(type)=>this.setState({itemType:type})}
                            underlineColorAndroid="transparent"

                            />
                    </View>
                </View> */}
                 <View style={{margin:10}} >
                    <Text style={{marginLeft:27.5,color:'white',marginBottom:5,fontSize:13}} >SELECT CATEGORY</Text>
                    <View style={{backgroundColor:'white',width:'80%',height:Platform.OS == 'ios' ? 25 : 28,marginLeft:Platform.OS == 'android' ? 28 : 29.5,borderRadius:3}}> 
                    {Platform.OS == 'android' ? 

                     this._category(this.state.itemType) : 

                    <TouchableOpacity onPress={() => this.setState({btn:true})}> 
                        <Text style={{margin:5}}>{this.state.category}</Text>
                    </TouchableOpacity> }
                    </View> 
                </View>
                <View style={{margin:10}}>
                    <Text style={{marginLeft:27.5,color:'white',marginBottom:5,fontSize:13}} >AUTO GENERATED CODE</Text>
                    <View style={{flexDirection:'row',width:'80%',height:25 ,marginLeft:Platform.OS == 'android' ? 28 : 29.5,borderRadius:3,backgroundColor:'white',alignItems:'center'}}>
                        <TextInput  
                            style={{width:200,height:40,marginLeft:5}} 
                           // onChangeText={()=>this.setState({itemName:name})}
                            underlineColorAndroid="transparent"

                            />
                    </View>
                </View>
                <View style={{margin:10}}>
                    <Text style={{marginLeft:27.5,color:'white',marginBottom:5,fontSize:13}} >ADD ITEM PRICE</Text>
                    <View style={{flexDirection:'row',width:'80%',height: 25 ,marginLeft:Platform.OS == 'android' ? 28 : 29.5,borderRadius:3,backgroundColor:'white',alignItems:'center'}}>
                        <TextInput  
                            style={{width:200,height:40,marginLeft:5}} 
                            onChangeText={(price)=>this.setState({itemPrice:price})}
                            underlineColorAndroid="transparent"

                            />
                    </View>
                </View>
                <View style={{margin:10}}>
                    <TouchableOpacity style={{width:110,padding:5,backgroundColor:'#000',marginLeft:29,alignItems:'center',justifyContent:'center',borderColor:'white',borderWidth:1}}
                    onPress={()=>this.addItems()} >
                        <Text style={{color:'white',fontSize:11,fontWeight:Platform.OS == 'android' ? '500' : '700'}}>SAVE CHANGE</Text>
                    </TouchableOpacity>
                </View>
               
            </View>
            </ScrollView>
            {
              this.state.btn2 == true ?
              <View style={{backgroundColor :'rgba(0,0,0,0.5)',position:'absolute',width:'100%',height:'100%'}}>
              <View style={{borderBottomWidth:1,top:'25%',backgroundColor:'rgba(256,256,256,0.8)',height:'30%'}}>
                    <Picker
                          selectedValue={this.state.itemType}
                          onValueChange={(itemValue, itemIndex) => this.setState({itemType: itemValue,btn2:false,category:itemValue == "Clothes" ? "Select" : itemValue})}>
                          <Picker.Item label="Select" value="Select" />
                          <Picker.Item label="Clothes" value="Clothes" />
                          <Picker.Item label="Carpets" value="Carpets" />
                          <Picker.Item label="Curtains" value="Curtains" />
                          <Picker.Item label="Shoes" value="Shoes" />
                          <Picker.Item label="Cover" value="Cover" />
                          <Picker.Item label="Blanket" value="Blanket" />
                    </Picker> 
              </View>
               </View>
   
            :
            this.state.btn == true ?
            <View style={{backgroundColor :'rgba(0,0,0,0.5)',position:'absolute',width:'100%',height:'100%'}}>
            <View style={{borderBottomWidth:1,top:'25%',backgroundColor:'rgba(256,256,256,0.8)',height:'30%'}}>
                
                 {this._category(this.state.itemType)} 
            </View>
             </View>
 
          : null
            }
        {/* <Tab/> */}
        <Modal
           animationType="slide"
           transparent={true}
           visible={this.state.modalVisible}
           onRequestClose={() => {this.setModalVisible(!this.state.modalVisible);}}>
           <View style={styles.modalview}>
             <View style={styles.modal}>
             <View style={{paddingLeft:'5%',paddingTop:'1%'}}>
                   <Text style={{fontSize:15,padding:5,color:'grey'}}>Select From</Text></View>
                <TouchableOpacity onPress={()=>this.Camera()} style={styles.modalbtn}><Icon name='camera' style={{color:'#2c8ef4',paddingRight:'2%'}}/><Text style={styles.modeltxt}>Camera</Text></TouchableOpacity>
                <TouchableOpacity onPress={()=>this. Gallery()} style={styles.modalbtn}><Icon name='md-images' style={{color:'#f42ced',paddingRight:'2%'}}/><Text style={styles.modeltxt}>Gallery</Text></TouchableOpacity>
               <TouchableOpacity style={styles.modalbtn}
                 onPress={()=>{this.setModalVisible(!this.state.modalVisible);}}>
                 <Icon name='close' style={{color:'#25de5b',paddingLeft:5,paddingRight:'2%'}}/><Text style={styles.modeltxt}>Cancel</Text>
               </TouchableOpacity>
             </View>
           </View>
         </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:20,
    fontWeight:Platform.OS == 'android' ? '500' :'900'
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  inputView:{
    flexDirection:'row',
    width:'66%',
    height:35, 
    marginLeft:20,
    borderRadius:3,
    backgroundColor:'white'
  },
  modeltxt:{
    fontSize:20,
    padding:'3%',
    paddingRight:'6%'
   },
   modalbtn:{
    paddingHorizontal:'2%',
     flexDirection:'row',
     alignItems:'center',
     paddingLeft:'5%'
   },
   modal: {
    height: '30%',
    width: '100%',
    backgroundColor:'white',
    borderRadius: 7,
    elevation:100,
   },
   modalview: {
    flex: 1,
    alignItems:'center',
    justifyContent:'flex-end',
   },
 
});
