import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage,
  TextInput,
  Alert
} from 'react-native';
import Api from '../../components/api';
import { Icon} from 'native-base';
import { Actions } from 'react-native-router-flux';
import Geocoder from 'react-native-geocoding';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

var lat
var long
var gpsclick=false
export default class MyAccount extends Component {
    constructor(props){
        super(props);
        this.state={
            userID:'',
            email:'',
            emailError:'',
            username:'',
            password:'',
            phoneNumber: '',
            address:'',
        }
    }
     componentWillMount(){
        if(Platform.OS === 'android'){
            LocationServicesDialogBox.checkLocationServicesIsEnabled({ 
              message: "<h2>Use Location?</h2> \
                          This app wants to change your device settings:<br/><br/>\
                          Use GPS for location<br/><br/>", 
              ok: "YES", 
              cancel: "NO" 
          }).then(() => { 
        navigator.geolocation.getCurrentPosition((position) => {
            lat = parseFloat(position.coords.latitude)
            long = parseFloat(position.coords.longitude)
           console.log('lat',lat)
           console.log('long',long)
           
              }, (error) => console.log(JSON.stringify(error)),
           { enableHighAccuracy: false, timeout: 20000 })

            })
        }
        else{
            navigator.geolocation.getCurrentPosition((position) => {
                lat = parseFloat(position.coords.latitude)
                long = parseFloat(position.coords.longitude)
               console.log('lat',lat)
               console.log('long',long)
               
                  }, (error) => console.log(JSON.stringify(error)),
               { enableHighAccuracy: false, timeout: 20000 })
        }
     }
     _getAddress(){
        gpsclick=true
        // fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+lat + ',' + long)
        //  .then((response) =>  response.json())
        //  .then((result) => {
            
        //      console.log(result)
        //      this.setState({address:result.results[0].formatted_address})
        //      console.log(this.state.address)
             
        // })

        Geocoder.init('AIzaSyCAOFImrlK9C7-wmmqxz44KTs8ebW8BCHg');
        if(Platform.OS === 'android'){
            LocationServicesDialogBox.checkLocationServicesIsEnabled({ 
              message: "<h2>Use Location?</h2> \
                          This app wants to change your device settings:<br/><br/>\
                          Use GPS for location<br/><br/>", 
              ok: "YES", 
              cancel: "NO" 
          }).then(() => {
        Geocoder.from(lat,long)
        .then(json => { console.log(json)
                var addressComponent = json.results[0].address_components[0];
                this.setState({address:json.results[0].formatted_address})
          console.log(addressComponent);
        })
        .catch(error => console.warn(error));
        })
    }
    else{
        Geocoder.from(lat,long)
        .then(json => { console.log(json)
                var addressComponent = json.results[0].address_components[0];
                this.setState({address:json.results[0].formatted_address})
          console.log(addressComponent);
        })
        .catch(error => {Alert.alert("Use Location?","Please Enable Your Location..!"),gpsclick=false});
    }
    
    }
  componentDidMount(){
    AsyncStorage.getItem('manager_id')
    .then(id => this.setState({userID:id}))

    AsyncStorage.getItem('email')
    .then(Email => {this.setState({email:Email}),console.log(Email)})

    AsyncStorage.getItem('user_name')
    .then(UserName => this.setState({username:UserName}))

    AsyncStorage.getItem('password')
    .then(Password =>{ this.setState({password:Password}),console.log(Password)})

    AsyncStorage.getItem('address')
    .then(Address => this.setState({address:Address}))

    AsyncStorage.getItem('phone_number')
    .then(Phone => this.setState({phoneNumber:Phone.slice(4)}))
}
    _update(){
        if(gpsclick!=true){
            Alert.alert('Use Location','Please enable your location service and use location icon in order to input your correct location.')
        }
        else{
        var formData = new FormData()
        formData.append('user_id',this.state.userID)
        formData.append('user_name',this.state.username);
        formData.append('email',this.state.email);
        formData.append('phone_number','+962'+this.state.phoneNumber);
        formData.append('password',this.state.password);
        formData.append('address',this.state.address);
   
        Api.post('app/api/updateMyAccount',formData)
        .then(result => {console.log('result',result)
    
            if(result.message == 'Success.')
            {
                 AsyncStorage.multiRemove(['user_id','email','user_name','password','address','phone_number'])
                 AsyncStorage.setItem('user_id',this.state.userID.toString())
                 AsyncStorage.setItem('email',this.state.email)
                 AsyncStorage.setItem('user_name',this.state.username)
                 AsyncStorage.setItem('password',this.state.password)
                 AsyncStorage.setItem('address',this.state.address)
                 AsyncStorage.setItem('phone_number','+962'+this.state.phoneNumber.toString())

                 alert('information updated successfully')
              
            }
            else
            {
                this.setState({emailError: result.message})
            }
    
        })
        .catch(error => {console.error(error)});
        }
     }

  render() {
    return (
      <View style={styles.container}>
        <View style={{backgroundColor:'white',alignItems:'center',width:'100%', flex:1,padding:10,marginTop:5}}> 
        <ScrollView style={{width:'100%'}} >
         <View style={{flexDirection:'row',width:'100%',marginTop:20}}>
            <View style={{marginTop:10}}>
                <View style={styles.titleView}>
                    <Text style={styles.title}>Email</Text>
                </View>
                <View style={styles.titleView}>
                    <Text style={styles.title}>Username</Text>
                </View>
                <View style={styles.titleView}>
                    <Text style={styles.title}>Password</Text>
                </View>
                <View style={styles.titleView}>
                    <Text style={styles.title}>Phone Number</Text>
                </View>
                <View style={styles.titleView}>
                    <Text style={styles.title}>Address</Text>
                </View>
            </View>
            <View style={{width:'100%',marginLeft:10,marginTop:10}}>
                <View>
                    <TextInput 
                        style={styles.textInput}
                        onChangeText={(text) => this.setState({email:text})}
                        value = {this.state.email}/>
                </View>
                <View>
                    <TextInput 
                        style={styles.textInput} 
                        onChangeText={(text) => this.setState({username:text})}
                        value = {this.state.username}/>
                </View>
                <View>
                    <TextInput 
                        style={styles.textInput}
                        onChangeText={(text) => this.setState({password:text})} 
                        value = {this.state.password}/>
                </View>
                <View style={{flexDirection:'row'}}>
                <Text style={{paddingTop:Platform.OS == 'android' ? 18 : 20}}>+962 | </Text>
                    <TextInput 
                           style={[styles.textInput,{width:'48%'}]}
                        onChangeText={(text) => this.setState({phoneNumber:text})} 
                        value = {this.state.phoneNumber}/>
                </View>
                <View style={{flexDirection:'row'}}>
                    <TextInput 
                        style={styles.textInput}
                        onChangeText={(text) => this.setState({address:text})} 
                        value = {this.state.address}/>
                    <TouchableOpacity onPress={()=> this._getAddress()} >
                        <Icon type="MaterialIcons" name="my-location" style={{padding:6,fontSize:22, color:'#ea0017',marginTop:10}} />
                    </TouchableOpacity>
                </View>
            </View>
         </View>
         <View style={{justifyContent:'center',alignItems:'center'}}>
            <TouchableOpacity style={styles.btn} onPress={() => this._update()} >
                        <Text style={{color:'white',fontSize:16,fontWeight:'500',textAlign:'center'}}>Save Change</Text>
            </TouchableOpacity>   
         </View>
        </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
   // alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'black',
    fontSize:13
  },
  titleView:{
    marginTop:10,
    height:40,
    justifyContent:'center'
  },
  textInput:{
    marginTop:10,
    width:'60%', 
    height:40
  },
  btn:{
    marginTop:20,
    backgroundColor:'#ea0017',
    height:40,
    width:'40%',
    justifyContent:'center',
    borderRadius:5
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:150,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  list: {
    flexDirection:'row',
    padding:15,
    borderBottomWidth:1,
    borderBottomColor:'grey',
    backgroundColor:'white'
  },
 
});
