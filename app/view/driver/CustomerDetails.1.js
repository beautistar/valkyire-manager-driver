
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  Linking
} from 'react-native';
import { Icon,Radio } from 'native-base';
import RadioButton from 'react-native-radio-button';
import { Actions } from 'react-native-router-flux';
var radio_props = [
  {label: 'Cash', value: 0 },
  {label: 'Visa', value: 1 }
];
export default class CustomerDetails extends Component {
  constructor(props){
    super(props);
    this.state={
      custName:this.props.cust_name,
      totalItem:this.propstotal_itmes,
      totalAmt:this.props.total_cost,
      phone:'1234567890'
    }
    console.log(this.props.cust_name)
  }
  static navigationOptions = {
    headerTitle:<View style={{width:'85%'}}><Image source={require('../../images/logo.png')} style={Platform.OS=='ios'?{width:60,height:42,alignSelf:'center'}:{width:85,height:70,alignSelf:'center'}}/></View>,
  }
  sendSMS(url){
     Linking.canOpenURL(url).then(supported => {
       if (!supported) {
        console.log('Can\'t handle url: ' + url);
       } else {
        return Linking.openURL(url);
       }
     }).catch(err => console.error('An error occurred', err));
   
  }
  render() {
    return (
      <View style={styles.container}>
      <ScrollView style={{width:'100%'}}>
      <View style={{marginTop:25,alignSelf:'center'}}> 
        <Text style={styles.title}>LORIUM IPSUM</Text>
      </View>
      <View style={{marginTop:30,padding:10,width:'100%'}}>
        <View style={{flexDirection:'row',alignItems:'center',margin:5}}>
              <Text style={{fontWeight:'500',color:'white',fontSize:12}}>Customer Name</Text>
              <View style={styles.inputView}>
              <Icon name="ios-person-outline" style={{marginLeft:5,padding:5,fontSize:20}}/>
                <TextInput  
                  style={{width:120,fontSize:12,color:'red',fontWeight:'bold'}}
                  placeholder="Customer Name" 
                  underlineColorAndroid="transparent"
                  onChangeText={(text)=>this.setState({custName:text})}
                  value={this.state.custName}/>
              </View>
            </View>
            <View style={{flexDirection:'row',alignItems:'center',margin:5}}>
              <Text style={{fontWeight:'500',color:'white',fontSize:12}}>Total Items</Text>
              <View style={{flexDirection:'row',width:'66%',height:35,marginLeft:Platform.OS == 'android' ? 45.5 : 49.5,borderRadius:3,backgroundColor:'white'}}>
                <Image source={require('../../images/products/items.png')} style={{height:15,width:15,alignSelf:'center',marginLeft:10 }} />
                <TextInput  
                  style={{width:120,fontSize:12,color:'red',fontWeight:'bold',marginLeft:Platform.OS == 'android' ? 2 :3}}
                  placeholder="Total Items" 
                  underlineColorAndroid="transparent"
                  onChangeText={(text)=>this.setState({totalItem:text})} 
                  value={this.state.totalItem}/>
              </View>
            </View>
            <View style={{flexDirection:'row',alignItems:'center',margin:5}}>
              <Text style={{fontWeight:'500',color:'white',fontSize:12}}>Total Amounts</Text>
              <View style={{flexDirection:'row',width:'32%',height:35,marginLeft:Platform.OS == 'android' ? 28 : 29.5,borderRadius:3,backgroundColor:'white'}}>
              <Image source={require('../../images/products/cash2.png')} style={{height:15,width:15,alignSelf:'center',marginLeft:10}} />
                <TextInput  
                  style={{width:120,fontSize:12,color:'red',fontWeight:'bold',marginLeft:Platform.OS == 'android' ? 2 :3}}
                  placeholder="Total Amount" 
                  underlineColorAndroid="transparent"
                  onChangeText={(text)=>this.setState({totalAmt:text})} 
                  value={this.state.totalAmt}/>
              </View>
              <View style={{flexDirection:'row',marginLeft:5,justifyContent:'center',alignItems:'center'}} >
                  {/* <Radio selected={true} radioColor={'white'} /><Text style={{fontWeight:'500',color:'white',fontSize:12,marginLeft:5}}>Cash</Text>
                  <Radio selected={false}  style={{marginLeft:5}}/><Text style={{fontWeight:'500',color:'white',fontSize:12,marginLeft:5}}>Visa</Text> */}
                 <RadioButton
                    size={10}
                    animation={'bounceIn'}
                    innerColor={'white'}
                    outerColor={'white'}
                    isSelected={true}
                  /><Text style={{fontWeight:'500',color:'white',fontSize:12,marginLeft:5,marginRight:5}}>Cash</Text>
                  <RadioButton
                    size={10}
                    animation={'bounceIn'}
                    innerColor={'white'}
                    outerColor={'white'}
                    isSelected={false}
                  /><Text style={{fontWeight:'500',color:'white',fontSize:12,marginLeft:5}}>Visa</Text>
              </View>
            </View>
      </View>
      <View style={{alignItems:'center',marginLeft:25}}>
        <TouchableOpacity style={{width:'45%',backgroundColor:'white',alignItems:'center',padding:5,borderRadius:3,marginTop:30}} onPress={()=> Actions.pop()}>
          <Text style={{fontSize:15,color:'red',fontWeight:Platform.OS == 'android' ? '500' : '800'}}>Confirm Items</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:'45%',backgroundColor:'white',alignItems:'center',padding:5,borderRadius:3,marginTop:30}} onPress={()=> Actions.pop()}>
          <Text style={{fontSize:15,color:'red',fontWeight:Platform.OS == 'android' ? '500' : '800'}}>Unconfirm Items</Text>
        </TouchableOpacity>
      </View>
        </ScrollView>
      <View style={{flexDirection:'row',backgroundColor:'white',bottom:0,height:60,width:'100%',alignItems:'center'}} >
        <TouchableOpacity style={{ flexDirection:'row',backgroundColor:'white',alignItems:'center',padding:18,borderRadius:3,width:'50%',borderRightWidth:1,borderRightColor:'red'}}  onPress={()=>this.sendSMS('tel:'+this.state.phone)}>
          <Icon name="phone-in-talk" type="MaterialIcons" style={{color:'red',fontSize:30}} />
          <Text style={{fontSize:16,color:'red',marginLeft:5}}>Call Customer</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{flexDirection:'row',backgroundColor:'white',alignItems:'center',padding:18,borderRadius:3,width:'50%'}} onPress={()=>this.sendSMS('sms:'+this.state.phone)}>
          <Icon name="ios-mail-outline" style={{color:'red',fontSize:32}} />
          <Text style={{fontSize:16,color:'red',marginLeft:5}}>SMS Customer</Text>
        </TouchableOpacity>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:20,
    fontWeight:Platform.OS == 'android' ? '500' :'900'
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  inputView:{
    flexDirection:'row',
    width:'66%',
    height:35, 
    marginLeft:20,
    borderRadius:3,
    backgroundColor:'white'
  },
 
});
