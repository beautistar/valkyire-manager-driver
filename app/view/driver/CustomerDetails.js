
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  Linking
} from 'react-native';
import { Icon,Radio } from 'native-base';
import RadioButton from 'react-native-radio-button';
import { Actions } from 'react-native-router-flux';
var radio_props = [
  {label: 'Cash', value: 0 },
  {label: 'Visa', value: 1 }
];
export default class CustomerDetails extends Component {
  constructor(props){
    super(props);
    this.state={
      custName:this.props.cust_name,
      totalItem:this.props.total_itmes,
      totalAmt:this.props.total_cost,
      phone:'1234567890'
    }
    console.log(this.props.total_itmes)
  }
  

  sendSMS(url){
    Actions.driverapp()
     Linking.canOpenURL(url).then(supported => {
       if (!supported) {
        console.log('Can\'t handle url: ' + url);
       } else {
        return Linking.openURL(url);
       }
     }).catch(err => console.error('An error occurred', err));
    
  }
  render() {
    return (
      <View style={styles.container}>
      <TouchableOpacity style={{justifyContent:'center',alignSelf:'flex-end',paddingRight:15}} onPress={this.props.close}>
        <Icon name="close" style={{color:'white',fontSize:Platform.OS == 'ios' ? 35 : 25,fontWeight:'bold'}} />
      </TouchableOpacity>
      <ScrollView style={{width:'100%'}}>
      <View style={{marginTop:5,alignSelf:'center'}}> 
        <Text style={styles.title}>LORIUM IPSUM</Text>
      </View>
      <View style={{padding:10,width:'100%'}}>
        <View style={{alignItems:'center',margin:5}}>
              <Text style={{fontWeight:'500',color:'white',fontSize:12}}>Customer Name</Text>
              <View style={styles.inputView}>
              <Icon name="ios-person-outline" style={{marginLeft:5,padding:5,fontSize:20}}/>
                <TextInput  
                  style={{marginLeft:5,width:'100%',fontSize:12,color:'red',fontWeight:'bold'}}
                  placeholder="Customer Name" 
                  editable={false}
                  underlineColorAndroid="transparent"
                  onChangeText={(text)=>this.setState({custName:text})}
                  value={this.state.custName}/>
              </View>
            </View>
            <View style={{alignItems:'center',margin:5}}>
              <Text style={{fontWeight:'500',color:'white',fontSize:12}}>Total Items</Text>
              <View style={styles.inputView}>
                <Image source={require('../../images/products/items.png')} style={{height:15,width:15,alignSelf:'center',marginLeft:10 }} />
                <TextInput  
                  style={{marginLeft:5,width:'100%',fontSize:12,color:'red',fontWeight:'bold'}}
                  placeholder="Total Items" 
                  editable={false}
                  underlineColorAndroid="transparent"
                  onChangeText={(text)=>this.setState({totalItem:text})} 
                  value={this.state.totalItem}/>
              </View>
            </View>
            <View style={{alignItems:'center',margin:5}}>
              <Text style={{fontWeight:'500',color:'white',fontSize:12}}>Total Amounts</Text>
              <View style={styles.inputView}>
              <Image source={require('../../images/products/cash2.png')} style={{height:15,width:15,alignSelf:'center',marginLeft:10}} />
                <TextInput  
                  style={{marginLeft:5,width:'100%',fontSize:12,color:'red',fontWeight:'bold'}}
                  placeholder="Total Amount" 
                  editable={false}
                  underlineColorAndroid="transparent"
                  onChangeText={(text)=>this.setState({totalAmt:text})} 
                  value={this.state.totalAmt}/>
              </View>
              <View style={{flexDirection:'row',marginLeft:5,justifyContent:'center',alignItems:'center',marginTop:10}} >
                  {/* <Radio selected={true} radioColor={'white'} /><Text style={{fontWeight:'500',color:'white',fontSize:12,marginLeft:5}}>Cash</Text>
                  <Radio selected={false}  style={{marginLeft:5}}/><Text style={{fontWeight:'500',color:'white',fontSize:12,marginLeft:5}}>Visa</Text> */}
                 <RadioButton
                    size={10}
                    animation={'bounceIn'}
                    innerColor={'white'}
                    outerColor={'white'}
                    isSelected={true}
                  /><Text style={{fontWeight:'500',color:'white',fontSize:12,marginLeft:5,marginRight:5}}>Cash</Text>
                  <RadioButton
                    size={10}
                    animation={'bounceIn'}
                    innerColor={'white'}
                    outerColor={'white'}
                    isSelected={false}
                  /><Text style={{fontWeight:'500',color:'white',fontSize:12,marginLeft:5}}>Visa</Text>
              </View>
            </View>
      </View>
      <View style={{alignItems:'center',padding:5}}>
        <TouchableOpacity style={{width:'80%',backgroundColor:'white',alignItems:'center',padding:5,borderRadius:3,marginTop:5}} onPress={this.props.confirm}>
          <Text style={{fontSize:15,color:'red',fontWeight:Platform.OS == 'android' ? '500' : '800'}}>{this.props.btn}</Text>
        </TouchableOpacity>
        {/* <TouchableOpacity style={{width:'80%',backgroundColor:'white',alignItems:'center',padding:5,borderRadius:3,marginTop:5}} onPress={this.props.close}>
          <Text style={{fontSize:15,color:'red',fontWeight:Platform.OS == 'android' ? '500' : '800'}}>Close</Text>
        </TouchableOpacity> */}
      </View>
        </ScrollView>
      <View style={{flexDirection:'row',backgroundColor:'white',width:'100%',alignItems:'center',marginTop:Platform.OS=='ios'?20: 14.5}} >
        <TouchableOpacity style={{flexDirection:'row',backgroundColor:'white',alignItems:'center',padding:18,width:'50%',borderRightWidth:1,borderRightColor:'red'}}  onPress={this.props.call}>
          <Icon name="phone-in-talk" type="MaterialIcons" style={{color:'red',fontSize:20}} />
          <Text style={{fontSize:11,color:'red',marginLeft:5}}>Call Customer</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{flexDirection:'row',backgroundColor:'white',alignItems:'center',padding:18,width:'50%'}} onPress={this.props.sms}>
          <Icon name="ios-mail-outline" style={{color:'red',fontSize:20}} />
          <Text style={{fontSize:11,color:'red',marginLeft:5}}>SMS Customer</Text>
        </TouchableOpacity>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop:5,
    marginBottom:-25,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ea0017',
  },
  title:{
    color:'white',
    fontSize:20,
    fontWeight:Platform.OS == 'android' ? '500' :'900'
  },
  box:{
    justifyContent:'center',
    width:'45%',
    margin:5,
    height:200,
    borderWidth:1,
    borderColor:'white'
  },
  boxText:{
    marginTop:15,
    textAlign:'center',
    color:'white',
    fontSize:15
  },
  activeBox:{
    justifyContent:'center',
    margin:5,
    width:'45%',
    height:200,
    backgroundColor:'white'
  },
  activeBoxText:{
    marginTop:15,
    textAlign:'center',
    color:'#ea0017',
    fontSize:15
  },
  image:{
    width:50,
    height:50,
    alignSelf:'center'
  },
  inputView:{
    flexDirection:'row',
    width:'66%',
    height:35, 
    marginLeft:20,
    borderRadius:3,
    backgroundColor:'white'
  },
 
});
